//
//  NNHKManager.m
//  NovoOiX
//
//  Created by ctby on 1/17/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "NNHKManager.h"
#import "NNOixCloud.h"
#import "NNHKUtils.h"
#import "NovoOixUserDefaults.h"

@interface NNHKManager()
{
    HKHealthStore* _healthStore;
    NSArray* _heartSamples;
    NSDate* _lastHeartSamplesFetchDate;
    NSArray* _nutritionSamples;
    NSDate* _lastNutritionSamplesFetchDate;
    NSArray* _stepSamples;
    NSDate* _lastStepSamplesFetchDate;
    NSArray* _workoutSamples;
    NSDate* _lastWorkoutSamplesFetchDate;
    NSArray* _nutritionTypes;
}
@end



@implementation NNHKManager

+(NNHKManager*) sharedManager {
    static NNHKManager* manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[NNHKManager alloc] init];
    });
    return manager;
}

-(instancetype) init
{
    if (self = [super init]) {
        _healthStore = [[HKHealthStore alloc] init];
        _nutritionTypes = @[HKQuantityTypeIdentifierDietaryFatTotal,
                                    HKQuantityTypeIdentifierDietaryCholesterol,
                                    HKQuantityTypeIdentifierDietarySodium,
                                    HKQuantityTypeIdentifierDietaryCarbohydrates,
                                    HKQuantityTypeIdentifierDietaryFiber,
                                    HKQuantityTypeIdentifierDietarySugar,
                                    HKQuantityTypeIdentifierDietaryEnergyConsumed,
                                    HKQuantityTypeIdentifierDietaryProtein,
                                    ];
    }
    return self;
}

- (BOOL)isHealthDataAvailableAndAuthorized {
    return  [HKHealthStore isHealthDataAvailable] && HKAuthorizationStatusSharingAuthorized == [_healthStore authorizationStatusForType:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount]];
}

- (void)requestAuthorizationWithCompletion:(void (^)(BOOL success, NSError * _Nullable error))completion
{
    [[NovoOixUserDefaults sharedDefaults] putRequestedHealthKitAccess:YES];
    if (![HKHealthStore isHealthDataAvailable]) {
        NSLog(@"ERROR: Health data is not available for this device");
        completion(FALSE, [[NSError alloc] initWithDomain:NSCocoaErrorDomain code:-1 userInfo:@{@"description":@"Health data is not available for this device."}]);
        return;
    }
    
    NSMutableArray* readTypes = [NSMutableArray array];
    [readTypes addObjectsFromArray:@[[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate],
                                     [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount],
                                     [HKObjectType workoutType]]];
    
    for (HKQuantityTypeIdentifier hkTypeId in _nutritionTypes) {
        HKObjectType *hkObj = [HKObjectType quantityTypeForIdentifier:hkTypeId];
        [readTypes addObject:hkObj];
    }
    NSArray *writeTypes = @[[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount],
                            [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate],
                            [HKObjectType workoutType]];
    
    [_healthStore requestAuthorizationToShareTypes:[NSSet setWithArray:writeTypes]
                                         readTypes:[NSSet setWithArray:readTypes]
                                        completion:^(BOOL success, NSError * _Nullable error) {
                                             NSLog(@"NNHKManager: authorization success = %d, error = %@", success, error);
                                             completion(success, error);
                                         }];
}

#pragma mark - Share data directly from HealthKit to an export format
- (void)shareHeartRateWithCompletion:(void(^)(NSArray *samples, NSError *error))completion {
    if (_heartSamples.count > 0 && _lastHeartSamplesFetchDate && ABS([_lastHeartSamplesFetchDate timeIntervalSinceNow]) < 10) {
        completion(_heartSamples, nil);
        return;
    }
    
    _lastHeartSamplesFetchDate = [NSDate date];
    [self fetchHeartRates:^(NSArray *samples, NSError *error) {
        _heartSamples = [self convertHeartrateSamples:samples];
        completion(_heartSamples, error);
    }];
}

- (void)shareStepsWithCompletion:(void(^)(NSArray *samples, NSError *error))completion {
    if (_stepSamples.count > 0 && _lastStepSamplesFetchDate && ABS([_lastStepSamplesFetchDate timeIntervalSinceNow]) < 10) {
        completion(_stepSamples, nil);
        return;
    }
    
    _lastStepSamplesFetchDate = [NSDate date];
    [self fetchSteps:^(NSArray *samples, NSError *error) {
        _stepSamples = [self convertStepSamples:samples];
        completion(_stepSamples, error);
    }];
}

- (void)shareWorkoutsWithCompletion:(void(^)(NSArray *samples, NSError *error))completion {
    if (_workoutSamples.count > 0 && _lastWorkoutSamplesFetchDate && ABS([_lastWorkoutSamplesFetchDate timeIntervalSinceNow]) < 10) {
        completion(_workoutSamples, nil);
        return;
    }
    
    _lastStepSamplesFetchDate = [NSDate date];
    [self fetchWorkouts:^(NSArray *samples, NSError *error) {
        _workoutSamples = [self convertWorkoutSamples:samples];
        completion(_workoutSamples, error);
    }];
}

- (void)shareNutritionWithCompletion:(void(^)(NSArray *samples, NSError *error))completion {
    if (_nutritionSamples.count > 0 && _lastNutritionSamplesFetchDate && ABS([_lastNutritionSamplesFetchDate timeIntervalSinceNow]) < 10) {
        completion(_nutritionSamples, nil);
        return;
    }
    
    _lastNutritionSamplesFetchDate = [NSDate date];
    [self fetchNutrition:^(NSArray *samples, NSError *error) {
        _nutritionSamples = [self convertNutritionSamples:samples];
        completion(_nutritionSamples, error);
    }];
}

#pragma mark - Push health kit data to cloud
- (void)pushHeartbeatsToOix:(void (^)(BOOL status, NSError *error))completion {
    [self fetchHeartRates:^(NSArray *samples, NSError *error) {
        [self pushHeartbeatSamplesToOix:samples completion:completion];
    }];
}

- (void)pushStepsToOix:(void (^)(BOOL status, NSError *error))completion {
    [self fetchSteps:^(NSArray *samples, NSError *error) {
        [self pushStepCountSamplesToOix:samples completion:completion];
    }];
}

-(void)pushWorkoutsToOiX:(void(^)(BOOL status, NSError* error))completion {
    [self fetchWorkouts:^(NSArray *samples, NSError *error) {
        [self pushWorkoutSamplesToOix:samples completion:completion];
    }];
}

#pragma mark - Fetch HealthKit data into samples

// Fetch the heartrate samples
- (void)fetchHeartRates:(void (^)(NSArray* samples, NSError *error))completion {
    
    //get the heart rates
    NSDate *endDate = [NSDate date];
    NSDate *startDate = [endDate dateByAddingTimeInterval: -(7*24*60*60)];
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierStartDate ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = @[lastDateDescriptor];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType
                                                           predicate:predicate
                                                               limit:HKObjectQueryNoLimit
                                                     sortDescriptors:sortDescriptors
                                                      resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){

                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              if (!results) {
                                                                  NSLog(@"An error occured fetching the user's heartrate. The error was: %@.", error);
                                                              }
                                                              if (completion) {
                                                                  completion(results, error);
                                                              }
                                                          });
                                                      }];
    [_healthStore executeQuery:query];
}

- (void)fetchSteps:(void (^)(NSArray* samples, NSError *error))completion {
    NSDate *endDate = [NSDate date];
    NSDate *startDate = [endDate dateByAddingTimeInterval: -(60*60*24*7)]; // 7 days
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    
    HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierStartDate ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = @[lastDateDescriptor];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType
                                                           predicate:predicate
                                                               limit:HKObjectQueryNoLimit
                                                     sortDescriptors:sortDescriptors
                                                      resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){
                                                          
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              if (!results) {
                                                                  NSLog(@"An error occured fetching the user's step count. The error was: %@.", error);
                                                              }
                                                              if (completion) {
                                                                  completion(results, error);
                                                              }
                                                          });
                                                      }];
    [_healthStore executeQuery:query];
}


-(void)fetchWorkouts:(void(^)(NSArray* samples, NSError* error))completion
{
    // 1. Predicate to read only running workouts
    //NSPredicate *predicate = [HKQuery predicateForWorkoutsWithWorkoutActivityType:HKWorkoutActivityTypeRunning];
    //HKWorkout  *workout = [HKWorkout workoutWithActivityType:HKWorkoutActivityTypeSwimming startDate:startDate endDate:endDate];
    //predicate = [HKQuery predicateForObjectsFromWorkout:workout];
    //NSPredicate *explicitWorkout = [NSPredicate predicateWithFormat:@"%K >= %d", HKPredicateKeyPathWorkoutDuration, 60 * 5];
    
    // 2. Order the workouts by date
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:HKSampleSortIdentifierStartDate ascending:false];
    
    // Predicate matching workouts equal to or longer than 5 minutes
    //NSPredicate* durationPredicate = [HKQuery predicateForWorkoutsWithOperatorType:NSGreaterThanOrEqualToPredicateOperatorType duration:60.0 * 5];
    NSDate *endDate = [NSDate date];
    NSDate *startDate = [endDate dateByAddingTimeInterval: -(7*24*60*60)];
    NSPredicate *datePredicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    
    
    // 3. Create the query
    HKSampleQuery *sampleQuery = [[HKSampleQuery alloc] initWithSampleType:[HKWorkoutType workoutType]
                                                                 predicate:datePredicate
                                                                     limit:HKObjectQueryNoLimit
                                                           sortDescriptors:@[sortDescriptor]
                                                            resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error)
                                  {
                                      if (completion) {
                                          completion(results, error);
                                      }
                                  }];
    
    // Execute the query
    [_healthStore executeQuery:sampleQuery];
}

- (void)fetchNutrition:(void (^)(NSArray* samples, NSError *error))completion {
    
    dispatch_group_t group = dispatch_group_create(); // create group
    
    //get the heart rates
    NSDate *endDate = [NSDate date];
    NSDate *startDate = [endDate dateByAddingTimeInterval: -(7*24*60*60)];
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierStartDate ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = @[lastDateDescriptor];
    __block NSMutableArray* samples = [NSMutableArray array];
    
    for (HKQuantityTypeIdentifier typeId in _nutritionTypes) {
        dispatch_group_enter(group);
        HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:typeId];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType
                                                               predicate:predicate
                                                                   limit:HKObjectQueryNoLimit
                                                         sortDescriptors:sortDescriptors
                                                          resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){
                                                              @synchronized(samples) {
                                                                  [samples addObjectsFromArray:results];
                                                              }
                                                              dispatch_group_leave(group);
                                                              
                                                          }];
        [_healthStore executeQuery:query];
    }
    
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(samples, nil);
            }
        });
    });
}

#pragma mark - Convert samples into an array of dictionaries, JSON-like structure
- (NSArray*) convertHeartrateSamples:(NSArray*)samples {
    if (samples == nil) {
        return nil;
    }
    NSMutableArray *samplesArray = [NSMutableArray array];
    
    for (HKQuantitySample *sample in samples) {
        if (sample.quantity) {
            double hbpm = [sample.quantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
            NSDictionary* postData = @{@"bpm" : @((int)hbpm),
                                       @"sampleDate" : @((long long) sample.startDate.timeIntervalSince1970 * 1000)
                                       };
            [samplesArray addObject:postData];
        }
    }
    return samplesArray;
}

- (NSArray*)convertStepSamples:(NSArray*)samples {
    NSMutableArray *samplesArray = [NSMutableArray arrayWithCapacity:(7*24)]; // a week's work of step data
    // Aggregate the steps into hours
    int hourInSeconds = 60*60;
    NSDate *curDate = nil;
    int stepCount = 0;
    
    for (HKQuantitySample *sample in samples) {
        if (sample.quantity) {
            double steps = [sample.quantity doubleValueForUnit:[HKUnit unitFromString:@"count"]];
            if (curDate == nil || (sample.startDate.timeIntervalSinceNow - curDate.timeIntervalSinceNow) < hourInSeconds) {
                if (curDate == nil) {
                    curDate = sample.startDate;
                }
                stepCount += steps;
            } else {
                //NSLog(@"curDate: %@ -> %@  diff=%0.2lf", curDate, sample.startDate, (sample.startDate.timeIntervalSinceNow - curDate.timeIntervalSinceNow)/(3600));
                // Add sample, start new count
                NSDictionary* stepPostData = @{@"steps" : @((int)stepCount),
                                               @"sampleDate" : @((long long) curDate.timeIntervalSince1970 * 1000)
                                               };
                [samplesArray addObject:stepPostData];
                
                // Start new
                curDate = sample.startDate;
                stepCount = steps;
            }
        }
    }
    
    // Add last sample
    if (curDate && stepCount > 0) {
        // Add sample, start new count
        NSDictionary* stepPostData = @{@"steps" : @((int)stepCount),
                                       @"sampleDate" : @((long long) curDate.timeIntervalSince1970 * 1000)
                                       };
        [samplesArray addObject:stepPostData];
    }
    return samplesArray;
}


- (NSArray*)convertNutritionSamples:(NSArray*)samples {
    if (samples == nil) {
        return nil;
    }
    NSMutableArray *samplesArray = [NSMutableArray array];
    
    for (HKQuantitySample *sample in samples) {
        if (sample.quantity) {
            double hbpm = 0;
            NSString* typeId = [NSString stringWithFormat:@"%@", sample.quantityType.identifier];
            if ([typeId isEqualToString:@"HKQuantityTypeIdentifierDietaryEnergyConsumed"]) {
                hbpm = [sample.quantity doubleValueForUnit:[HKUnit unitFromString:@"J"]];
            } else {
                hbpm = [sample.quantity doubleValueForUnit:[HKUnit unitFromString:@"g"]];
            }
            
            if (hbpm == 0) {
                // skip it if the value is zero
                NSLog(@"Skipping zero value: %@ for food type: %@", typeId, sample.metadata[HKMetadataKeyFoodType]);
                continue;
            }

            typeId = [typeId stringByReplacingOccurrencesOfString:@"HKQuantityTypeIdentifier" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, typeId.length-1)];
            NSString* foodType = sample.metadata[HKMetadataKeyFoodType]?:@"unknown";
            NSDictionary* postData = @{@"units" : @(hbpm),
                                       @"typeId" : typeId,
                                       @"foodType" : foodType,
                                       @"sampleDate" : @((long long) sample.startDate.timeIntervalSince1970 * 1000)
                                       };
            [samplesArray addObject:postData];
        }
    }
    return samplesArray;
}

- (NSArray*)convertWorkoutSamples:(NSArray*)samples {
    NSMutableArray *samplesArray = [NSMutableArray array];
    
    for(HKWorkout *workout in samples) {
        double totalDistanceInMeters = [NNHKUtils distanceInMetersForQuantity:workout.totalDistance];
        double totalEnergyBurned = [NNHKUtils energyBurnedForQuantity:workout.totalEnergyBurned];
        NSString* workoutName = [NNHKUtils workoutNameForType:workout.workoutActivityType];
        NSDate *startDate =  workout.startDate;
        NSDate *endDate =  workout.endDate;
        int duration = workout.duration;
        unsigned int workoutType = (unsigned int)workout.workoutActivityType;
        NSDictionary* workoutData = @{@"workoutName" : workoutName,
                                      @"startDate"   : @((long long) startDate.timeIntervalSince1970 * 1000),
                                      @"endDate"     : @((long long) endDate.timeIntervalSince1970 * 1000),
                                      @"duration"    : @((int)duration),
                                      @"workoutType" : @((unsigned int)workoutType),
                                      @"totalEnergyBurned": @((int)totalEnergyBurned),
                                      @"totalDistance" : @((int)totalDistanceInMeters)
                                      };
        
        [samplesArray addObject:workoutData];
    }
    return samplesArray;
}

#pragma mark - Push converted samples to Cloud
// Push data from HK -> NNOiX DynamoDB
-(void)pushWorkoutSamplesToOix:(NSArray*)samples completion:(void(^)(BOOL status, NSError* error))completion
{
    NSArray *samplesArray = [self convertWorkoutSamples:samples];
    
    if (samplesArray.count > 0) {
        [[NNOixCloud sharedCloud] addWorkouts:samplesArray completion:completion];
    } else if (completion) {
        completion(YES,nil);
    }
}

-(void)pushHeartbeatSamplesToOix:(NSArray*)samples completion:(void(^)(BOOL status, NSError* error))completion
{
    NSArray *samplesArray =  [self convertHeartrateSamples:samples];

    if (samplesArray.count > 0) {
        [[NNOixCloud sharedCloud] addHeartbeats:samplesArray completion:^(BOOL status, NSError *error) {
            NSLog(@"Completed pushing heartrate up to Oix for sampleCount=%lu, lastError=%@", (unsigned long)samplesArray.count, error);
            if (completion) {
                completion((error == nil), error);
            }
        }];
        
    } else if (completion) {
        completion(YES,nil);
    }
}

-(void)pushStepCountSamplesToOix:(NSArray*)samples completion:(void(^)(BOOL status, NSError* error))completion
{
    NSArray* samplesArray = [self convertStepSamples:samples];
    if (samplesArray.count > 0) {
        [[NNOixCloud sharedCloud] addStepCounts:samplesArray completion:completion];
    } else if (completion) {
        completion(YES,nil);
    }
}

// Deprecated. Too much data, must be batched
//-(void) pushHeartbeatSampleToOix:(HKQuantitySample*)sample completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion {
//    double hbpm = [sample.quantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
//    [[NNOixCloud sharedCloud] addHeartbeatWithBPM:hbpm sampleDate:sample.startDate completion:completion];
//}
//
//-(void) pushStepCountSampleToOix:(HKQuantitySample*)sample completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion {
//    if (sample.quantity) {
//        double steps = [sample.quantity doubleValueForUnit:[HKUnit unitFromString:@"count"]];
//        [[NNOixCloud sharedCloud] addStepCount:steps sampleDate:sample.startDate completion:completion];
//    } else {
//        completion(nil, nil);
//    }
//}


#pragma Mock Data Add

- (void)addWorkoutDataForType:(HKWorkoutActivityType)activityType
                    startDate:(NSDate*)startDate
                      endDate:(NSDate*)endDate
                   completion:(void(^)(BOOL success, NSError * _Nullable error))completion
{
    //    NSDate *endDate = [NSDate date];
    //    NSDate *startDate = [endDate dateByAddingTimeInterval: -(60*60*3)]; // 3 hours
    HKQuantity *distance = nil; //[HKQuantity quantityWithUnit:[HKUnit mileUnit] doubleValue:1.2];
    HKQuantity *energyBurned = nil; //[HKQuantity quantityWithUnit:[HKUnit kilocalorieUnit] doubleValue:320.0];
    NSDictionary *metadata = nil; //@{HKMetadataKeyIndoorWorkout: @(YES)};
    
    HKWorkout *workout = [HKWorkout workoutWithActivityType:activityType
                                                  startDate:startDate
                                                    endDate:endDate
                                                   duration:0
                                          totalEnergyBurned:energyBurned
                                              totalDistance:distance
                                                   metadata:metadata];
    
    [_healthStore saveObject:workout withCompletion:^(BOOL success, NSError *error) {
        
        if (!success) {
            // Perform proper error handling here...
            NSLog(@"*** An error occurred while saving this workout: %@ ***", error.localizedDescription);
        } else {
            NSLog(@"Successfully added workout data");
        }
        completion(success, error);
    }];
}


- (void)addHeartMockData {
    // 1. Create a heart rate BPM Sample
    NSDate* startDate = [NSDate date];
    NSDate* endDate = [NSDate date];
    HKQuantityType* heartRateType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    HKQuantity* heartRateQuantity = [HKQuantity quantityWithUnit:[HKUnit unitFromString:@"count/min"] doubleValue:(double)(arc4random_uniform(80) +100)];
    HKQuantitySample* heartSample = [HKQuantitySample quantitySampleWithType:heartRateType quantity:heartRateQuantity startDate:startDate endDate:endDate];
    
    [_healthStore saveObject:heartSample withCompletion:^(BOOL success, NSError *error) {
        if (!success) {
            // Perform proper error handling here...
            NSLog(@"*** An error occurred while saving this "
                  @"workout: %@ ***", error.localizedDescription);
        }
    }];
}


@end
