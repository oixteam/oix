//
//  NNHKManager.h
//  NovoOiX
//
//  Created by ctby on 1/17/18.
//  Copyright © 2018 ctby. All rights reserved.
//

@import Foundation;
@import UIKit;
@import HealthKit;


@interface NNHKManager : NSObject

+(NNHKManager*_Nonnull) sharedManager;

- (BOOL)isHealthDataAvailableAndAuthorized;
- (void)requestAuthorizationWithCompletion:(void (^_Nullable)(BOOL success, NSError * _Nullable error))completion;

- (void)addWorkoutDataForType:(HKWorkoutActivityType)activityType
                    startDate:(NSDate*_Nullable)startDate
                      endDate:(NSDate*_Nullable)endDate
                   completion:(void(^ _Nullable )(BOOL success, NSError * _Nullable error))completion;

- (void)pushWorkoutsToOiX:(void(^_Nullable)(BOOL status, NSError* _Nullable error))completion;
- (void)pushHeartbeatsToOix:(void (^_Nullable)(BOOL status, NSError * _Nullable error))completion;
- (void)pushStepsToOix:(void (^_Nullable)(BOOL status, NSError * _Nullable error))completion;

- (void)addHeartMockData;

// Direct share from HealthKit, use these, HK -> samples array
- (void)shareHeartRateWithCompletion:(void(^_Nonnull)(NSArray * _Nullable samples, NSError * _Nullable error))completion;
- (void)shareStepsWithCompletion:(void(^_Nullable)(NSArray * _Nullable samples, NSError * _Nullable error))completion;
- (void)shareWorkoutsWithCompletion:(void(^_Nullable)(NSArray * _Nullable samples, NSError * _Nullable error))completion;
- (void)shareNutritionWithCompletion:(void(^_Nullable)(NSArray * _Nullable samples, NSError * _Nullable error))completion;

@end
