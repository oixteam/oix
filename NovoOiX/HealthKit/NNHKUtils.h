//
//  NNHKUtils.h
//  NovoOiX
//
//  Created by ctby on 2/8/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HealthKit/HealthKit.h>

@interface NNHKUtils : NSObject

+(NSString*)workoutNameForType:(HKWorkoutActivityType)workoutType; // type -> name string
+(NSString*)emojiForType:(HKWorkoutActivityType)workoutType; // type -> emoji string
+(NSString*)workoutNameAndEmojiForType:(HKWorkoutActivityType) workoutType; // type -> emoji+name
+(NSArray*)allWorkoutTypes; // All the HK workout activity type enums

+(double) distanceInMetersForQuantity:(HKQuantity*)q;
+(double) energyBurnedForQuantity:(HKQuantity*)q;

@end
