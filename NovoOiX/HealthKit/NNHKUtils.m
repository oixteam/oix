//
//  NNHKUtils.m
//  NovoOiX
//
//  Created by ctby on 2/8/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "NNHKUtils.h"

static NSDictionary* NNHKActivityTypeMap;

static NSString* kName = @"name";
static NSString* kEmoji = @"emoji";

@implementation NNHKUtils

+(NSDictionary*) workoutMap {
    if (!NNHKActivityTypeMap) {
        // TOOL TIP to get the emoji popup : ctl+cmd+{space}
        NNHKActivityTypeMap = @{
                                @(HKWorkoutActivityTypeAmericanFootball): @{kName:@"Football", kEmoji:@"🏈"},
                                @(HKWorkoutActivityTypeArchery): @{kName:@"Archery", kEmoji:@"🏹"},
                                @(HKWorkoutActivityTypeAustralianFootball): @{kName:@"Austrailian Football", kEmoji:@"🏉"},
                                @(HKWorkoutActivityTypeBadminton): @{kName:@"Badminton", kEmoji:@"🏸"},
                                @(HKWorkoutActivityTypeBaseball): @{kName:@"Baseball", kEmoji:@"⚾️"},
                                @(HKWorkoutActivityTypeBasketball): @{kName:@"Basketball", kEmoji:@"🏀"},
                                @(HKWorkoutActivityTypeBowling): @{kName:@"Bowling", kEmoji:@"🎳"},
                                @(HKWorkoutActivityTypeBoxing): @{kName:@"Boxing", kEmoji:@"🥊"},
                                @(HKWorkoutActivityTypeClimbing): @{kName:@"Climbing", kEmoji:@"🧗🏻‍♀️"},
                                @(HKWorkoutActivityTypeCricket): @{kName:@"Cricket", kEmoji:@"🏏"},
                                @(HKWorkoutActivityTypeCrossTraining): @{kName:@"Cross Training", kEmoji:@"🤹🏻‍♀️"},
                                @(HKWorkoutActivityTypeCurling): @{kName:@"Curling", kEmoji:@"🥌"},
                                @(HKWorkoutActivityTypeCycling): @{kName:@"Cycling", kEmoji:@"🚴‍♀️"},
                                @(HKWorkoutActivityTypeDance): @{kName:@"Dance", kEmoji:@"💃🏻"},
                                //@(HKWorkoutActivityTypeDanceInspiredTraining): @{kName:@"Dance Inspired Training", kEmoji:@""},
                                @(HKWorkoutActivityTypeElliptical): @{kName:@"Elliptical", kEmoji:@"🕺🏻"},
                                @(HKWorkoutActivityTypeEquestrianSports): @{kName:@"Equestrian Sports", kEmoji:@"🏇"},
                                @(HKWorkoutActivityTypeFencing): @{kName:@"Fencing", kEmoji:@"🤺"},
                                @(HKWorkoutActivityTypeFishing): @{kName:@"Fishing", kEmoji:@"🎣"},
                                @(HKWorkoutActivityTypeFunctionalStrengthTraining): @{kName:@"Functional Strength Training", kEmoji:@"🏋🏻‍♀️"},
                                @(HKWorkoutActivityTypeGymnastics): @{kName:@"Gymnastics", kEmoji:@"🤸🏻‍♀️"},
                                @(HKWorkoutActivityTypeHandball): @{kName:@"Handball", kEmoji:@"🤾🏻‍♂️"},
                                @(HKWorkoutActivityTypeHiking): @{kName:@"Hiking", kEmoji:@"🧗🏻‍♀️"},
                                @(HKWorkoutActivityTypeHockey): @{kName:@"Hockey", kEmoji:@"🏒🥅"},
                                @(HKWorkoutActivityTypeHunting): @{kName:@"Hunting", kEmoji:@""},
                                @(HKWorkoutActivityTypeLacrosse): @{kName:@"Lacrosse", kEmoji:@""},
                                @(HKWorkoutActivityTypeMartialArts): @{kName:@"Martial Arts", kEmoji:@""},
                                @(HKWorkoutActivityTypeMindAndBody): @{kName:@"Mind and Body", kEmoji:@""},
                                //@(HKWorkoutActivityTypeMixedMetabolicCardioTraining): @{kName:@"Mixed Metabolic Cardio Training", kEmoji:@""},
                                @(HKWorkoutActivityTypePaddleSports): @{kName:@"Paddle Sports", kEmoji:@""},
                                @(HKWorkoutActivityTypePlay): @{kName:@"Play", kEmoji:@"🎈"},
                                @(HKWorkoutActivityTypePreparationAndRecovery): @{kName:@"Preparation and Recovery", kEmoji:@""},
                                @(HKWorkoutActivityTypeRacquetball): @{kName:@"Racquetball", kEmoji:@"💙"},
                                @(HKWorkoutActivityTypeRowing): @{kName:@"Rowing", kEmoji:@""},
                                @(HKWorkoutActivityTypeRugby): @{kName:@"Rugby", kEmoji:@""},
                                @(HKWorkoutActivityTypeRunning): @{kName:@"Running", kEmoji:@"🏃🏻‍♀️"},
                                @(HKWorkoutActivityTypeSailing): @{kName:@"Sailing", kEmoji:@"⛵️"},
                                @(HKWorkoutActivityTypeSkatingSports): @{kName:@"Skating Sports", kEmoji:@""},
                                @(HKWorkoutActivityTypeSnowSports): @{kName:@"Snow Sports", kEmoji:@"❄️"},
                                @(HKWorkoutActivityTypeSoccer): @{kName:@"Soccer", kEmoji:@"⚽️"},
                                @(HKWorkoutActivityTypeSoftball): @{kName:@"Softball", kEmoji:@"⚾️"},
                                @(HKWorkoutActivityTypeSquash): @{kName:@"Squash", kEmoji:@"🤾🏻‍♂️"},
                                @(HKWorkoutActivityTypeStairClimbing): @{kName:@"Stair Climbing", kEmoji:@""},
                                @(HKWorkoutActivityTypeSurfingSports): @{kName:@"Surfing Sports", kEmoji:@"🏄🏻‍♀️"},
                                @(HKWorkoutActivityTypeSwimming): @{kName:@"Swimming", kEmoji:@"🏊🏻‍♀️"},
                                @(HKWorkoutActivityTypeTableTennis): @{kName:@"Table Tennis", kEmoji:@"🏓"},
                                @(HKWorkoutActivityTypeTennis): @{kName:@"Tennis", kEmoji:@"🎾"},
                                @(HKWorkoutActivityTypeTrackAndField): @{kName:@"Track and Field", kEmoji:@"🏃🏻‍♀️"},
                                @(HKWorkoutActivityTypeTraditionalStrengthTraining): @{kName:@"Traditional Strength Training", kEmoji:@"🏋🏻‍♀️"},
                                @(HKWorkoutActivityTypeVolleyball): @{kName:@"Volleyball", kEmoji:@"🏐"},
                                @(HKWorkoutActivityTypeWalking): @{kName:@"Walking", kEmoji:@"🚶🏻‍♀️"},
                                @(HKWorkoutActivityTypeWaterFitness): @{kName:@"Water Fitness", kEmoji:@"🤽🏻‍♀️"},
                                @(HKWorkoutActivityTypeWaterPolo): @{kName:@"Water Polo", kEmoji:@"🤽🏻‍♀️"},
                                @(HKWorkoutActivityTypeWaterSports): @{kName:@"Water Sports", kEmoji:@"🏊🏻‍♀️"},
                                @(HKWorkoutActivityTypeWrestling): @{kName:@"Wrestling", kEmoji:@"🤼‍♀️"},
                                @(HKWorkoutActivityTypeYoga): @{kName:@"Yoga", kEmoji:@"🧘🏻‍♀️"},
                                @(HKWorkoutActivityTypeBarre): @{kName:@"Barre", kEmoji:@"🧘🏻‍♀️"},
                                @(HKWorkoutActivityTypeCoreTraining): @{kName:@"Core Training", kEmoji:@""},
                                @(HKWorkoutActivityTypeCrossCountrySkiing): @{kName:@"Cross Country Skiing", kEmoji:@""},
                                @(HKWorkoutActivityTypeDownhillSkiing): @{kName:@"Downhill Skiing", kEmoji:@"⛷"},
                                @(HKWorkoutActivityTypeFlexibility): @{kName:@"Flexibility", kEmoji:@""},
                                @(HKWorkoutActivityTypeHighIntensityIntervalTraining): @{kName:@"High Intensity Interval Training", kEmoji:@""},
                                @(HKWorkoutActivityTypeJumpRope): @{kName:@"Jump Rope", kEmoji:@""},
                                @(HKWorkoutActivityTypeKickboxing): @{kName:@"Kickboxing", kEmoji:@"🥊"},
                                @(HKWorkoutActivityTypePilates): @{kName:@"Pilates", kEmoji:@""},
                                @(HKWorkoutActivityTypeSnowboarding): @{kName:@"Snowboarding", kEmoji:@"🏂"},
                                @(HKWorkoutActivityTypeStairs): @{kName:@"Stairs", kEmoji:@"🎢"},
                                @(HKWorkoutActivityTypeStepTraining): @{kName:@"Step Training", kEmoji:@""},
                                @(HKWorkoutActivityTypeWheelchairWalkPace): @{kName:@"Wheelchair Walk Pace", kEmoji:@""},
                                @(HKWorkoutActivityTypeWheelchairRunPace): @{kName:@"Wheelchair Run Pace", kEmoji:@""},
                                @(HKWorkoutActivityTypeTaiChi): @{kName:@"Tai Chi", kEmoji:@""},
                                @(HKWorkoutActivityTypeMixedCardio): @{kName:@"Mixed Cardio", kEmoji:@""},
                                @(HKWorkoutActivityTypeHandCycling): @{kName:@"Hand Cycling", kEmoji:@""},
                                @(HKWorkoutActivityTypeOther): @{kName:@"Other", kEmoji:@""},
                                };
    }
    return NNHKActivityTypeMap;
}

+(NSString*)workoutNameForType:(HKWorkoutActivityType)workoutType {
    return self.workoutMap[@(workoutType)][kName]?:@"unknown";
}

+(NSString*)emojiForType:(HKWorkoutActivityType)workoutType {
    return self.workoutMap[@(workoutType)][kEmoji]?:@"🚴‍♀️";
}

+ (NSString*)workoutNameAndEmojiForType:(HKWorkoutActivityType) workoutType {
    NSString* workoutName = [NSString stringWithFormat:@"%@%@", [NNHKUtils emojiForType:workoutType], [NNHKUtils workoutNameForType:workoutType]];
    return workoutName;
}

+(NSArray*)allWorkoutTypes {
    return [self workoutMap].allKeys;
}


+(double) distanceInMetersForQuantity:(HKQuantity*)q {
    return [q doubleValueForUnit:[HKUnit meterUnit]];
}
+(double) energyBurnedForQuantity:(HKQuantity*)q {
    return [q doubleValueForUnit:[HKUnit kilocalorieUnit]];
}

@end
