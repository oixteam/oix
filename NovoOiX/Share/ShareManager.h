//
//  ShareManager.h
//  NovoOiX
//
//  Created by ctby on 3/6/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

typedef NS_ENUM(NSUInteger, ShareFileTypes) {
    kShare_CSV =  (1<<0),
    kShare_JSON = (1<<1),
    kShare_PDF = (1<<2),
};

static NSString* ShareManagerKey_fileBaseName = @"fileBaseName";
static NSString* ShareManagerKey_jsonArray = @"jsonArray";


@interface ShareManager : NSObject

+ (ShareManager *)sharedManager;

- (void)shareData:(NSArray*)shareArray options:(ShareFileTypes)options fromView:(UIViewController*)parentVC completion:(void (^)(void))completion;

@end
