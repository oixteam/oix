//
//  ShareManager.m
//  NovoOiX
//
//  Created by ctby on 3/6/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "ShareManager.h"
#import <UIKit/UIKit.h>
@import CoreText;

@implementation ShareManager

+ (ShareManager *)sharedManager {
    static ShareManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ShareManager alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if ( (self = [super init]) ) {
        
    }
    return self;
}

#pragma mark - share json

- (void)shareData:(NSArray*)shareArray options:(ShareFileTypes)options fromView:(UIViewController*)parentVC completion:(void (^)(void))completion {
    
    if (shareArray.count == 0 || options == 0) {
        if (completion) {
            completion();
        }
        return;
    }
    
    NSMutableArray *activityItems = [NSMutableArray arrayWithCapacity:shareArray.count];
    
    for (NSDictionary* map in shareArray) {
        NSString* fileBaseName = map[ShareManagerKey_fileBaseName];
        NSArray* jsonArray = map[ShareManagerKey_jsonArray];
        if (fileBaseName == nil || jsonArray == nil) {
            NSLog(@"Incorrect share array, skipping: %@", map);
        } else {
            NSURL *localFileURL = nil;
            if ([self convertToCSV:options]) {
                NSString* fileName = [fileBaseName stringByAppendingPathExtension:@"csv"];
                localFileURL = [self writeCSV:jsonArray toFile:fileName];
                if (localFileURL) {
                    [activityItems addObject:localFileURL];
                }
            }
            if ([self convertToPDF:options]) {
                NSString* fileName = [fileBaseName stringByAppendingPathExtension:@"pdf"];
                localFileURL = [self writePDF:jsonArray toFile:fileName];
                if (localFileURL) {
                    [activityItems addObject:localFileURL];
                }
            }
            if ([self convertToJSON:options]) {
                NSString* fileName = [fileBaseName stringByAppendingPathExtension:@"txt"];
                localFileURL = [self writeJson:jsonArray toFile:fileName];
                if (localFileURL) {
                    [activityItems addObject:localFileURL];
                }
            }
        }
    }
    
    if (activityItems.count > 0) {
        [activityItems addObject:@"NovoOiX health data"];
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        [parentVC presentViewController:activityViewController animated:YES completion:^{
            NSLog(@"Completed sharing!");
            if (completion) {
                completion();
            }
        }];
    } else {
        if (completion) {
            completion();
        }
    }
}

- (BOOL)convertToCSV:(ShareFileTypes)options {
    return (options & kShare_CSV) != 0;
}
- (BOOL)convertToPDF:(ShareFileTypes)options {
    return (options & kShare_PDF) != 0;
}
- (BOOL)convertToJSON:(ShareFileTypes)options {
    return (options & kShare_JSON) != 0;
}

- (NSURL*) writeJson:(NSArray*)json toFile:(NSString*)fileName {
    
    NSURL* localFileURL=nil;
    if (json) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        if (jsonData) {
            localFileURL = [self writeData:jsonData toFile:fileName];
        } else {
            NSLog(@"ERROR: could not parse json data: %@", error);
        }
    }
    return localFileURL;
}

- (NSURL*) writeCSV:(NSArray*)jsonData toFile:(NSString*)fileName {
    NSURL* localFileURL=nil;
    NSArray *keys= nil;
    // Get the headers
    NSMutableString* csv = [NSMutableString string];
    for (NSDictionary* map in jsonData) {
        if (keys == nil || csv.length == 0) {
            // Get the headers
            keys = map.allKeys;
            for (NSString* key in keys) {
                if (csv.length > 0) {
                    [csv appendString:@","];
                }
                NSString *stringVal= [NSString stringWithFormat:@"\"%@\"", key];
                [csv appendString:stringVal];
            }
            [csv appendString:@"\n"];
        }
        
        BOOL first=YES;
        for (NSString* key in keys) {
            if (first) {
                first = NO;
            } else {
                [csv appendString:@","];
            }
            NSString *stringVal= [NSString stringWithFormat:@"\"%@\"", map[key]];
            [csv appendString:stringVal];
        }
        [csv appendString:@"\n"];
    }
    
    localFileURL = [self writeData:[csv dataUsingEncoding:kCFStringEncodingUTF8] toFile:fileName];
    
    return localFileURL;
}

- (NSURL*) writePDF:(NSArray*)json toFile:(NSString*)fileName {
    
    NSURL* localFileURL=nil;
    if (json) {
        NSError *error;
        NSMutableData *pdfData = [NSMutableData data];
            
            //Create an NSAttributedString for CoreText. If you find a way to translate
            //PDF into an NSAttributedString, you can skip this step and simply use an
            //NSAttributedString for this method's argument.
            
            NSAttributedString* string = [[NSAttributedString alloc] initWithString: [json description]];
            
            //612 and 792 are the dimensions of the paper in pixels. (8.5" x 11")
            CGRect paperRect = CGRectMake(0.0, 0.0, 612, 792);
            
            CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef) string);
            CGSize requiredSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [string length]), NULL, CGSizeMake(paperRect.size.width - 144, 1e40), NULL);
            
            //Subtract the top and bottom margins (72 and 72), so they aren't factored in page count calculations.
            NSUInteger pageCount = ceill(requiredSize.height / (paperRect.size.height - 144));
            CFIndex resumePageIndex = 0;
            UIGraphicsBeginPDFContextToData(pdfData, paperRect, nil);
            
            for(NSUInteger i = 0; i < pageCount; i++)
            {
                
                //After calculating the required number of pages, break up the string and
                //draw them into sequential pages.
                
                UIGraphicsBeginPDFPage();
                CGContextRef currentContext = UIGraphicsGetCurrentContext();
                CGContextSaveGState (currentContext);
                CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
                CGMutablePathRef framePath = CGPathCreateMutable();
                
                //72 and 72 are the X and Y margins of the page in pixels.
                CGPathAddRect(framePath, NULL, CGRectInset(paperRect, 72.0, 72.0));
                
                CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, CFRangeMake(resumePageIndex, 0), framePath, NULL);
                resumePageIndex += CTFrameGetVisibleStringRange(frameRef).length;
                CGPathRelease(framePath);
                CGContextTranslateCTM(currentContext, 0, paperRect.size.height);
                CGContextScaleCTM(currentContext, 1.0, -1.0);
                CTFrameDraw(frameRef, currentContext);
                CFRelease(frameRef);
                CGContextRestoreGState (currentContext);
            }
            CFRelease(framesetter);
            UIGraphicsEndPDFContext();

        if (pdfData) {
            localFileURL = [self writeData:pdfData toFile:fileName];
        } else {
            NSLog(@"ERROR: could not parse json data: %@", error);
        }
    }
    return localFileURL;
}

- (NSURL*) writeData:(NSData*)data toFile:(NSString*)fileName {
    NSURL* localFileURL=nil;
    if (data) {
        NSString* pathComponent = [NSString stringWithFormat:@"Documents/%@", fileName];
        NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:pathComponent];
        
        //if ([fileName.pathExtension isEqualToString:@"pdf"]) {
            [data writeToFile:docPath atomically:YES];
//        } else {
//            NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            [jsonString writeToFile:docPath
//                         atomically:YES
//                           encoding:NSUTF8StringEncoding
//                              error:NULL];
//        }
        localFileURL = [NSURL fileURLWithPath:docPath];
    } else {
        NSLog(@"ERROR: writeData: Data is empty");
    }
    return localFileURL;
}

@end
