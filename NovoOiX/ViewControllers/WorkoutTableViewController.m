//
//  WorkoutTableViewController.m
//  NovoOiX
//
//  Created by ctby on 2/1/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "WorkoutTableViewController.h"
#import <HealthKit/HealthKit.h>

@interface WorkoutTableViewController ()
{
    NSArray* _workouts;
}

@end

static NSString* kWorkoutTypeKey = @"workoutType";
static NSString* kWorkoutStringKey = @"workoutString";

@implementation WorkoutTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _workouts = @[
                  @{kWorkoutTypeKey   : @(HKWorkoutActivityTypeRunning), kWorkoutStringKey : @"Running"},
                  @{kWorkoutTypeKey   : @(HKWorkoutActivityTypeWalking), kWorkoutStringKey : @"Walking"},
                  ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _workouts.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* reuseId = @"workoutReuseId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
    }
    NSDictionary* workoutInfo = _workouts[indexPath.row];
    cell.detailTextLabel.text = workoutInfo[kWorkoutStringKey];
    
    return cell;
}



#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}
*/

@end
