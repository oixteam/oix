//
//  SetupViewController.m
//  NovoOiX
//
//  Created by ctby on 3/7/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "SetupViewController.h"
#import "LoginViewController.h"
#import "ToSViewController.h"
#import "AccessSettingsViewController.h"
#import "NovoOixUserDefaults.h"
#import "NNOixCloud.h"
#import "NNHKManager.h"

@interface SetupViewController ()
{
    IBOutlet UIButton *_startButton;
    IBOutlet UIButton *_hkButton;
    IBOutlet UIButton *_registerButton;
    IBOutlet UIButton *_tosButton;
    
    BOOL _isLoggedIn;
    BOOL _requestedHealthAppPermissions;
    BOOL _tosAgreed;
    BOOL _isFirstRun;
}

@end

@implementation SetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self checkSetup];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self checkSetup];
}

- (void)checkSetup {
    _isLoggedIn = [[NNOixCloud sharedCloud] isSignedIn];
    _requestedHealthAppPermissions = /*[[NovoOixUserDefaults sharedDefaults] hasRequestedHealthKitAccess] ||*/ [[NNHKManager sharedManager] isHealthDataAvailableAndAuthorized];
    _tosAgreed = [[NovoOixUserDefaults sharedDefaults] agreedToTermsOfService];
    _isFirstRun = [[NovoOixUserDefaults sharedDefaults] isFirstRun];
    if (_isFirstRun) {
        [[NovoOixUserDefaults sharedDefaults] putFirstRun:NO];
    }
    _tosButton.enabled = !_tosAgreed;
    _hkButton.enabled = (!_requestedHealthAppPermissions);
    _registerButton.enabled = (!_isLoggedIn);
    
    if (/*_isLoggedIn && _requestedHealthAppPermissions &&*/ _tosAgreed) {
        _startButton.enabled = YES;
    } else {
        _startButton.enabled = NO;
    }
}

- (void)dismissLoginAndShowProfile {
    [self dismissViewControllerAnimated:NO completion:^{
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        UITabBarController *tabView = [storyboard instantiateViewControllerWithIdentifier:@"RootTabBarController"];
//        [self presentViewController:tabView animated:YES completion:nil];
    }];
    
    
}

- (IBAction)hkAccess:(id)sender {
    [self launchView:@"AccessSettingsViewController"];
    
}
- (IBAction)registerUser:(id)sender {
    [self launchView:@"LoginViewController"];
}
- (IBAction)agreeToTos:(id)sender {
    [self launchView:@"ToSViewController"];
}
- (IBAction)useApp:(id)sender {
    [self dismissLoginAndShowProfile];
}

- (void)launchView:(NSString*)vcName {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:vcName];
    
    if (self.navigationController) {
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self presentViewController:vc animated:YES completion:nil];
    }
}


@end
