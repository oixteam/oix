//
//  SettingsViewController.m
//  NovoOiX
//
//  Created by ctby on 3/6/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "SettingsViewController.h"
#import "NNHKManager.h"
#import "NNOixCloud.h"
#import "NovoOixUserDefaults.h"
#import "UIUtils.h"

@interface SettingsViewController ()
{
    IBOutlet UITableViewCell *_signInTableViewCell;
    IBOutlet UITableViewCell *_healthAccessTableViewCell;
    IBOutlet UITableViewCell *_termsTableViewCell;
    IBOutlet UITableViewCell *_advancedSettingsTableViewCell;
    IBOutlet UITableViewCell *_syncTableViewCell;
    IBOutlet UITableViewCell *_aboutTableViewCell;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _aboutTableViewCell.detailTextLabel.text = [UIUtils versionString];
}

- (void)viewDidAppear:(BOOL)animated {
    UITableViewCell *cell =_signInTableViewCell;
    NSString* userId = [[NNOixCloud sharedCloud] currentUserId];
    if (userId == nil) {
        cell.textLabel.text = @"Sign In";
        cell.detailTextLabel.text = @"Get a user id to push data to the cloud";
    } else {
        cell.textLabel.text = @"Sign Out";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"id: %@",userId];
    }
    
    cell = _healthAccessTableViewCell;
    if ([[NNHKManager sharedManager] isHealthDataAvailableAndAuthorized]) {
        cell.detailTextLabel.text = @"Disable sync of HealthKit to OiX Cloud";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        cell.detailTextLabel.text = @"Enable sync of HealthKit to OiX Cloud";
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    
    
    // CTBY: Disable cloud for final version
    _advancedSettingsTableViewCell.hidden = YES;
    [self updateAdvancedFeatures];
}

- (void) updateAdvancedFeatures {
    if ([[NovoOixUserDefaults sharedDefaults] boolForKey:NNPref_AdvancedFeatures]) {
        _advancedSettingsTableViewCell.textLabel.text = @"Disable Advanced Features";
        _advancedSettingsTableViewCell.hidden = NO;
        _signInTableViewCell.hidden = NO;
        _syncTableViewCell.hidden = NO;
    } else {
        _advancedSettingsTableViewCell.textLabel.text = @"Enable Advanced Features";
        _signInTableViewCell.hidden = YES;
        _syncTableViewCell.hidden = YES;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4) {
        BOOL advancedFeatures = [[NovoOixUserDefaults sharedDefaults] boolForKey:NNPref_AdvancedFeatures];
        [[NovoOixUserDefaults sharedDefaults] saveBoolean:!advancedFeatures forKey:NNPref_AdvancedFeatures];
        [self updateAdvancedFeatures];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

@end
