//
//  MockDataViewController.m
//  NovoOiX
//
//  Created by ctby on 2/15/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "MockDataViewController.h"
#import "NNHKManager.h"
#import "PopupViewController.h"

@interface MockDataViewController () <PopupViewControllerDelegate> {
    UIImageView* _heartPopup;
}

@end

@implementation MockDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)addHeartData:(id)sender {
    [[NNHKManager sharedManager] addHeartMockData];
    //[self showAlertWithTitle:@"❤️" message:@"Added heart data..."];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self animateHeartbeat];
    });
    
}

- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) animateHeartbeat {
    
    if (!_heartPopup) {
        UIImage* heartImage = [UIImage imageNamed:@"AppIcon86x86"];
        CGRect heartFrame = CGRectMake(self.view.frame.size.width/2 - 43, self.view.frame.size.height/2 - 43, 86, 86);

        _heartPopup = [[UIImageView alloc] initWithFrame:heartFrame];
        _heartPopup.image = heartImage;
        [self.view addSubview:_heartPopup];
    }
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _heartPopup.transform = CGAffineTransformMakeScale(1.3, 1.3);
        _heartPopup.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            _heartPopup.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                _heartPopup.transform = CGAffineTransformMakeScale(1.3, 1.3);
                _heartPopup.alpha = 0.0;
            } completion:^(BOOL finished) {
                _heartPopup.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }];
    }];
}
- (IBAction)rateApp:(id)sender {
    PopupViewController* vc = [[PopupViewController alloc] initWithNibName:@"PopupViewController" bundle:[NSBundle mainBundle]];
    vc.delegate = self;

//    vc.providesPresentationContextTransitionStyle = YES;
//    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    //PopupViewController* vc =(PopupViewController*) [[[NSBundle mainBundle] initWithNibName:@"PopupViewController"] lastObject];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];

}



- (void)ratingLevel:(NSInteger)rating {
    NSLog(@"rating level = %ld", rating);
}



@end
