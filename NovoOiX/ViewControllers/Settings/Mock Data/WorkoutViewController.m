//
//  WorkoutViewController.m
//  NovoOiX
//
//  Created by ctby on 1/17/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "WorkoutViewController.h"
#import "NNHKManager.h"
#import "NNHKUtils.h"


#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view
#define kDatePickerTag              99     // view tag identifiying the date picker view
#define kWorkoutPickerTag           98

#define kTitleKey       @"title"   // key for obtaining the data source item's title
#define kDateKey        @"date"    // key for obtaining the data source item's date value
#define kStringKey      @"string" // key for obtaining string key
#define kWorkoutTypeKey @"workoutType"

// keep track of which rows have date cells
#define kDateStartRow   1
#define kDateEndRow     2
#define kWorkoutRow     3

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kWorkoutPickerID = @"workoutPicker"; // the cell containing the date picker
static NSString *kWorkoutCellID = @"workoutCell";
static NSString *kOtherCell = @"basicCell";     // the remaining cells at the end

static NSString* kWorkoutTitle   = @"Workout Type";
static NSString* kStartTimeTitle = @"Start Time";
static NSString* kEndTimeTitle   = @"End Time";


typedef NS_ENUM(NSInteger, WorkoutCellType) {
    WorkoutCellType_Type=0,
    WorkoutCellType_StartTime=1,
    WorkoutCellType_EndTime=2
};

@interface WorkoutViewController () <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UITableView *_tableView;
    
    NSIndexPath * _lastSelectedRowIndexPath;
    NSDate *_startTime;
    NSDate *_endTime;
    HKWorkoutActivityType _workoutType;
    
    NSInteger _workoutPickerCellRowHeight; // Keep track of the inline picker height
    NSInteger _pickerCellRowHeight;
    NSIndexPath *_workoutPickerIndexPath; // When the pickers are visible, the index path var will be set
    NSIndexPath *_datePickerIndexPath;
    NSArray* _workoutItems;               // Items for the workout picker
    NSArray* _items;                      // Table items, an array of dictionaries
    NSDateFormatter* _dateFormatter;      // For the date picker results
}

@end

@implementation WorkoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _workoutItems = [NNHKUtils allWorkoutTypes];
    
    // setup our data source
    
    NSDate* now = [NSDate date];
    _startTime = now;
    _endTime = now;
    
    // Setup the date picker
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"MM/dd/YY h:mm a"];
    
    NSLog(@"date = %@", [_dateFormatter stringFromDate:now]);
    
    NSString* workoutName = [NNHKUtils workoutNameAndEmojiForType:HKWorkoutActivityTypeWalking];
    NSMutableDictionary *itemOne = [@{ kTitleKey : @"Tap a cell to change its setting:" } mutableCopy];
    NSMutableDictionary *itemTwo = [@{ kTitleKey : @"Start Date",
                                       kDateKey : _startTime } mutableCopy];
    NSMutableDictionary *itemThree = [@{ kTitleKey : @"End Date",
                                         kDateKey : _endTime } mutableCopy];
    NSMutableDictionary *itemWorkout = [@{ kTitleKey : @"Workout Type",
                                           kStringKey : workoutName,
                                           kWorkoutTypeKey : @(HKWorkoutActivityTypeWalking)
                                           } mutableCopy];

    _items = @[itemOne, itemTwo, itemThree, itemWorkout];
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    _pickerCellRowHeight = CGRectGetHeight([_tableView dequeueReusableCellWithIdentifier:kDatePickerID].frame);
    
    _workoutPickerCellRowHeight = [_tableView dequeueReusableCellWithIdentifier:kWorkoutPickerID].frame.size.height;
    
    // if the local changes while in the background, we need to be notified so we can update the date
    // format in the table view cells
    //
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localeChanged:)
                                                 name:NSCurrentLocaleDidChangeNotification
                                               object:nil];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSCurrentLocaleDidChangeNotification
                                                  object:nil];
}


#pragma mark - Locale

/*! Responds to region format or locale changes.
 */
- (void)localeChanged:(NSNotification *)notif
{
    // the user changed the locale (region format) in Settings, so we are notified here to
    // update the date format in the table view cells
    //
    [_tableView reloadData];
}


#pragma mark - Utilities


- (BOOL)hasInlineDatePicker
{
    return (_datePickerIndexPath != nil);
}
- (BOOL)hasInlineWorkoutPicker
{
    return (_workoutPickerIndexPath != nil);
}

- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineDatePicker] && _datePickerIndexPath.row == indexPath.row);
}
- (BOOL)indexPathHasWorkoutPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineWorkoutPicker] && _workoutPickerIndexPath.row == indexPath.row);
}
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) ||
        (indexPath.row == kDateEndRow || ([self hasInlineDatePicker] && (indexPath.row == kDateEndRow + 1))))
    {
        hasDate = YES;
    }
    
    return hasDate;
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height  = _tableView.rowHeight;
    if ([self indexPathHasPicker:indexPath]) {
        height = _pickerCellRowHeight;
    } else if ([self indexPathHasWorkoutPicker:indexPath]) {
        height = _workoutPickerCellRowHeight;
    }
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // if we have a date picker, so allow for it in the number of rows in this section
    return _items.count + ((int)[self hasInlineDatePicker] + (int) [self hasInlineWorkoutPicker]) ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    NSString *cellID = kOtherCell;
    
    if ([self indexPathHasPicker:indexPath])
    {
        // the indexPath is the one containing the inline date picker
        cellID = kDatePickerID;     // the current/opened date picker cell
    }
    else if ([self indexPathHasDate:indexPath])
    {
        // the indexPath is one that contains the date information
        cellID = kDateCellID;       // the start/end date cells
    }
    else if (indexPath.row == kWorkoutRow) {
        cellID = kWorkoutCellID;
    }
    else if ([self indexPathHasWorkoutPicker:indexPath])
    {
        cellID = kWorkoutPickerID;
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (indexPath.row == 0)
    {
        // we decide here that first cell in the table is not selectable (it's just an indicator)
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // if we have a date picker open whose cell is above the cell we want to update,
    // then we have one more cell than the model allows
    //
    NSInteger modelRow = indexPath.row;
    if ((_datePickerIndexPath != nil && _datePickerIndexPath.row <= indexPath.row) ||
        (_workoutPickerIndexPath != nil && _workoutPickerIndexPath.row <= indexPath.row))
    {
        modelRow--;
    }
    
    NSDictionary *itemData = _items[modelRow];
    
    // proceed to configure our cell
    if ([cellID isEqualToString:kDateCellID])
    {
        // we have either start or end date cells, populate their date field
        //
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.detailTextLabel.text = [_dateFormatter stringFromDate:[itemData valueForKey:kDateKey]];
    }
    else if ([cellID isEqualToString:kWorkoutCellID])
    {
        HKWorkoutActivityType activityType = ((NSNumber*)itemData[kWorkoutTypeKey]).integerValue;
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.detailTextLabel.text = [NNHKUtils workoutNameAndEmojiForType:activityType];
    }
    else if ([cellID isEqualToString:kOtherCell])
    {
        // this cell is a non-date cell, just assign it's text label
        //
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
    }
    
    return cell;
}


- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [_tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ( [[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row+1 inSection:0]] viewWithTag:kDatePickerTag] != nil ||
        [[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row+1 inSection:0]] viewWithTag:kWorkoutPickerTag] != nil)
    {
        // found a picker below it, so remove it
        [_tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [_tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [_tableView endUpdates];
}

- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [_tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker])
    {
        before = _datePickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (_datePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_datePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        _datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        _datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [_tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    if (_datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [_tableView cellForRowAtIndexPath:_datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            //
            NSDictionary *itemData = _items[_datePickerIndexPath.row - 1];
            [targetedDatePicker setDate:[itemData valueForKey:kDateKey] animated:NO];
        }
    }
}

- (void)displayInlineWorkoutPickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [_tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineWorkoutPicker])
    {
        before = _workoutPickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (_workoutPickerIndexPath.row - 1 == indexPath.row);
    BOOL deletedDatePicker = NO;
    
    // remove any date picker cell if it exists
    if ([self hasInlineWorkoutPicker])
    {
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_workoutPickerIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        _workoutPickerIndexPath = nil;
    }
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_datePickerIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        _datePickerIndexPath = nil;
        deletedDatePicker = YES;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger row = indexPath.row;
        if (deletedDatePicker) {
            row = row - 1;
        }
        NSInteger rowToReveal = (before ? row - 1 : row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        _workoutPickerIndexPath  = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [_tableView endUpdates];
    
    // inform our date picker of the current value to match
    if (_workoutPickerIndexPath != nil)
    {
        UITableViewCell *cell = [_tableView cellForRowAtIndexPath:_workoutPickerIndexPath];
        UIPickerView *picker = (UIPickerView *)[cell viewWithTag:kWorkoutPickerTag];
        if (picker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value

            HKWorkoutActivityType workoutType = (HKWorkoutActivityType) _items[_workoutPickerIndexPath.row - 1];
            int pickerRow = 0;
            // TODO:
            //NSString* selectedString = NNHKActivityTypeMap[workoutType];

//            switch (workoutType) {
//                case HKWorkoutActivityTypeCycling :
//                    pickerRow = 3;
//                    break;
//                default:
//                    pickerRow = 0;
//            }
            [picker selectRow:pickerRow inComponent:0 animated:YES];
        }
    }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.reuseIdentifier == kDateCellID)
    {
        [self displayInlineDatePickerForRowAtIndexPath:indexPath];
    }
    else if (cell.reuseIdentifier == kWorkoutCellID)
    {
        [self displayInlineWorkoutPickerForRowAtIndexPath:indexPath];
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    //    RxInfo* info = [_items objectForKey:_sectionTitles[indexPath.section]][indexPath.row];
    //
    //    switch (info.rxType) {
    //        case NNRxDoseAmount:
    //            [self showDoseAmountDialogWithInfo:info];
    //            break;
    //        case NNRxDoseTime:
    //            //[self showDoseTimeDialogWithInfo:info];
    //            [self displayInlineDatePickerForRowAtIndexPath:indexPath];
    //            break;
    //        case NNRxBGTarget:
    //            [self showTargetBGDialogWithInfo:info];
    //            break;
    //        case NNRxDoseDays:
    //            [self performSegueWithIdentifier:@"doseAdjustmentDaysSegueId" sender:self];
    //            break;
    //        case NNRxDoseAdjustments:
    //            [self performSegueWithIdentifier:@"doseAdjustmentSegueId" sender:self];
    //            break;
    //    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Actions

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (IBAction)dateAction:(id)sender
{
    NSIndexPath *targetedCellIndexPath = [NSIndexPath indexPathForRow:_datePickerIndexPath.row - 1 inSection:0];
    
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    // update our data model
    NSMutableDictionary *itemData = _items[targetedCellIndexPath.row];
    [itemData setValue:targetedDatePicker.date forKey:kDateKey];
    
    if (targetedCellIndexPath.row == kDateEndRow) {
        _endTime = targetedDatePicker.date;
        if ([_endTime laterDate:_startTime] == _startTime) {
            _startTime = _endTime;
            NSMutableDictionary *startItem = _items[kDateStartRow];
            [startItem setValue:_startTime forKey:kDateKey];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kDateStartRow inSection:0];
            UITableViewCell *startCell = [_tableView cellForRowAtIndexPath:indexPath];
            startCell.detailTextLabel.text = [_dateFormatter stringFromDate:_startTime];
        }
    } else if (targetedCellIndexPath.row == kDateStartRow) {
        _startTime = targetedDatePicker.date;
        //if ([_startTime earlierDate:_endTime] == _endTime) {
            _endTime = [_startTime dateByAddingTimeInterval:60*30];
            NSMutableDictionary *endItem = _items[kDateEndRow];
            [endItem setValue:_endTime forKey:kDateKey];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kDateEndRow+1 inSection:0];
            UITableViewCell *startCell = [_tableView cellForRowAtIndexPath:indexPath];
            startCell.detailTextLabel.text = [_dateFormatter stringFromDate:_endTime];
        //}
    }
    
    
    // update the cell's date string
    cell.detailTextLabel.text = [_dateFormatter stringFromDate:targetedDatePicker.date];
}

#pragma mark - UIPickerViewDataSource - data

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _workoutItems.count;
}

#pragma mark - UIPickerViewDelegate - appearance

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSNumber* num = _workoutItems[row];
    return [NNHKUtils workoutNameAndEmojiForType:num.integerValue];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // Update the text on the workout cell
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(_workoutPickerIndexPath.row - 1) inSection:0]];
    NSNumber* num = _workoutItems[row];
    cell.detailTextLabel.text = [NNHKUtils workoutNameAndEmojiForType:num.integerValue];
    _items[kWorkoutRow][kWorkoutTitle] = cell.detailTextLabel.text;
    _items[kWorkoutRow][kWorkoutTypeKey] = @(num.integerValue);
}

- (IBAction)addWorkoutData:(id)sender {
    
    if (_datePickerIndexPath || _workoutPickerIndexPath) {
        [_tableView beginUpdates];
        if (_datePickerIndexPath) {
            [_tableView deleteRowsAtIndexPaths:@[_datePickerIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            _datePickerIndexPath = nil;
        }
        if (_workoutPickerIndexPath) {
            [_tableView deleteRowsAtIndexPaths:@[_workoutPickerIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            _workoutPickerIndexPath = nil;
        }
        [_tableView endUpdates];
    }
    
    NSMutableDictionary *startItem = _items[kDateStartRow];
    NSMutableDictionary *endItem = _items[kDateEndRow];
    NSMutableDictionary *workoutItem = _items[kWorkoutRow];

    [[NNHKManager sharedManager] addWorkoutDataForType:((NSNumber*)workoutItem[kWorkoutTypeKey]).unsignedIntegerValue startDate:startItem[kDateKey] endDate:endItem[kDateKey] completion:^(BOOL success, NSError *error) {
        [self showAlertWithTitle:@"Activity Data" message:@"👍 Successfully added activity data."];
    }];
}

- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}


@end
