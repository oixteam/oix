//
//  AccessSettingsViewController.m
//  NovoOiX
//
//  Created by ctby on 3/6/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "AccessSettingsViewController.h"
#import "NNHKManager.h"
#import "UIUtils.h"

@interface AccessSettingsViewController ()
{
    IBOutlet UISwitch *_enabledSwitch;
    
}

@end

@implementation AccessSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _enabledSwitch.on = [self isAuthorized];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)appDidBecomeActive:(NSNotification *)notification {
    _enabledSwitch.on = [self isAuthorized];
}

- (IBAction)allowHealthDataSync:(id)sender {
    
    if (_enabledSwitch.isOn) {
        [[NNHKManager sharedManager]  requestAuthorizationWithCompletion:^(BOOL success, NSError * _Nullable error) {
            // Always returns YES for success even though user has denied access
            dispatch_async(dispatch_get_main_queue(), ^{
                _enabledSwitch.on = [self isAuthorized];
                if (!_enabledSwitch.on) {
                    //[self showAlertWithTitle:@"Allow Health App Access" message:@"You must turn on all categories to allow data sync between Health App and the cloud"];
                    [UIUtils openHealthKitConfirmationFromVC:self];
                }
            });
        }];
    } else {
        [UIUtils openHealthKitConfirmationFromVC:self];
    }
}

- (BOOL) isAuthorized {
    return [[NNHKManager sharedManager] isHealthDataAvailableAndAuthorized];
}

@end
