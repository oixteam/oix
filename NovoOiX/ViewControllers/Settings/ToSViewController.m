//
//  ToSViewController.m
//  NovoOiX
//
//  Created by ctby on 3/7/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "ToSViewController.h"
#import "NovoOixUserDefaults.h"
@import WebKit;

static NSString* kTosURL = @"http://www.oi-x.dtu.dk/Challenges/Novo_2";

@interface ToSViewController () <WKNavigationDelegate>
{
    IBOutlet UISwitch *_tosSwitch;
    IBOutlet WKWebView *_webView;
    IBOutlet UIActivityIndicatorView *_spinnerView;
    
}

@end

@implementation ToSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLRequest* req = [NSURLRequest requestWithURL:[NSURL URLWithString:kTosURL]];
    _webView.navigationDelegate = self;
    [_webView loadRequest:req];
    _tosSwitch.on =   [[NovoOixUserDefaults sharedDefaults] agreedToTermsOfService];
}

- (IBAction)toggleAgree:(id)sender {
    [[NovoOixUserDefaults sharedDefaults] putAgreedToTermsOfService:_tosSwitch.isOn];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [_spinnerView stopAnimating];
}

@end
