//
//  LoginViewController.m
//  NovoOiX
//
//  Created by ctby on 3/2/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "LoginViewController.h"
#import "NNOixCloud.h"
#import "NovoOixUserDefaults.h"
#import "AppDelegate.h"

@import GoogleSignIn;

@interface LoginViewController () <GIDSignInUIDelegate, GIDSignInDelegate>
{
    
    IBOutlet UIButton *_loginButton;
    IBOutlet UILabel *_headerLabel;
    IBOutlet UILabel *_userIdLabel;
    IBOutlet GIDSignInButton *_googleSignIn;
    NSString* _userId;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Google signin
    [GIDSignIn sharedInstance].clientID = @"317737974015-1kq7etknomjvj1k75ten1p0clrk70qij.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [self updateButtons];
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(useTesterId:)];
    [_loginButton addGestureRecognizer:longPress];
    
}


- (void)useTesterId:(UILongPressGestureRecognizer*)gesture {
    if (![[NNOixCloud sharedCloud] isSignedIn] &&  gesture.state == UIGestureRecognizerStateEnded ) {
        [[NNOixCloud sharedCloud] useTesterId];
        [self updateButtons];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateButtons];
}

- (IBAction)registerAnonymously:(id)sender {
     if ([[NNOixCloud sharedCloud] isSignedIn]) {
         [self didTapSignOut:sender];
     } else {
        [[NNOixCloud sharedCloud] registerUserWithId:nil email:nil userName:@"anonymous" completion:^(NSDictionary *jsonResponse, NSError *error) {
            NSString* userId = jsonResponse[@"userId"];
            if (userId != nil) {
                [self updateButtons];
            } else {
                NSString* msg = [NSString stringWithFormat:@"ERROR: %@ %@", jsonResponse, error];
                [self showAlertWithTitle:@"Could not register" message:msg];
            }
        }];
     }
}

- (void)updateButtons {
   
    dispatch_async(dispatch_get_main_queue(), ^{
        _userId = [[NNOixCloud sharedCloud] currentUserId];
        _googleSignIn.hidden = (_userId != nil);
        if (_userId) {
            [_loginButton setTitle:@"Sign Out" forState:UIControlStateNormal];
            _userIdLabel.text = _userId;
        } else {
            [_loginButton setTitle:@"Register Anonymously" forState:UIControlStateNormal];
            _userIdLabel.text = @"not set";
        }
    });
}

- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)didTapSignOut:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
    [[NNOixCloud sharedCloud] signOut];
    [self updateButtons];
}

// gdsignin methods
/////////////
- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if (error) {
        // show error
        return;
    }

    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    //NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    //    NSString *givenName = user.profile.givenName;
    //    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    // [[NNOixCloud sharedCloud] signedInWithId:userId idToken:idToken];
    [[NNOixCloud sharedCloud] registerUserWithId:userId email:email userName:fullName completion:^(NSDictionary *jsonResponse, NSError *error) {
        NSString* userId = jsonResponse[@"userId"];
        if (userId != nil) {
            [self updateButtons];
        } else {
            NSString* msg = [NSString stringWithFormat:@"ERROR: %@ %@", jsonResponse, error];
            [self showAlertWithTitle:@"Could not register" message:msg];
        }
    }];
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}

@end
