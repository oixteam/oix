//
//  AboutViewController.m
//  NovoOiX
//
//  Created by ctby on 3/27/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "AboutViewController.h"
#import "UIUtils.h"
#import "NovoOixUserDefaults.h"
#import "UIUtils.h"

@interface AboutViewController ()
{
    IBOutlet UILabel *_versionLabel;
    IBOutlet UIImageView *_oixImageView;
    
}

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _versionLabel.text = [UIUtils versionString];
    
    // Disabled for now
//    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(enableAdvancedFeature:)];
//    [_oixImageView addGestureRecognizer:longPress];
}

- (void) enableAdvancedFeature:(UILongPressGestureRecognizer*)gesture {
    if ( gesture.state == UIGestureRecognizerStateEnded && ![[NovoOixUserDefaults sharedDefaults] boolForKey:NNPref_AdvancedFeatures]) {
        [[NovoOixUserDefaults sharedDefaults] saveBoolean:YES forKey:NNPref_AdvancedFeatures];
        [UIUtils showAlertWithTitle:@"Advanced Features" message:@"Advanced features have been enabled. 🌟" fromVC:self];
    }
}

@end
