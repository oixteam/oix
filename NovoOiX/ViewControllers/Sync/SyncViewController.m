//
//  SyncViewController.m
//  NovoOiX
//
//  Created by ctby on 1/17/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "SyncViewController.h"
#import "NNOixCloud.h"
#import "NNHKManager.h"
#import "ShareManager.h"
#import "NNHKUtils.h"
#import "NovoOixUserDefaults.h"
#import "ShowExportViewController.h"
#import "UIUtils.h"


@interface SyncViewController () <ShowExportViewControllerDelegate>
{
    IBOutlet UITextView *_statusTextView;
    IBOutlet UIButton *_showButton;
    IBOutlet UIButton *_syncButton;
    IBOutlet UIActivityIndicatorView *_spinnerView;
    NNHKManager* _hkManager;
    IBOutlet UIButton *_printButton;
    NSString* _currentFilter;
    BOOL _debugPrint;
    NSArray* _lastWorkoutJsonResponse;
    NSArray* _lastHeartbeatJsonResponse;
    NSArray* _lastStepsJsonResponse;
    BOOL _hkAccessRequested;
    IBOutlet UIImageView *_arrowImageView;
    IBOutlet UIView *_syncView;
    
    ShareFileTypes _shareOptions;
}

@end


static NSString* NNOiX_Key_userId = @"userId";
static NSString* NNOiX_Key_duration = @"duration";
static NSString* NNOiX_Key_startDate = @"startDate";
static NSString* NNOiX_Key_endDate = @"endDate";
static NSString* NNOiX_Key_totalDistance = @"totalDistance";
static NSString* NNOiX_Key_totalEnergyBurned = @"totalEnergyBurned";
static NSString* NNOiX_Key_workoutName = @"workoutName";
static NSString* NNOiX_Key_workoutType = @"workoutType";
static NSString* NNOiX_Key_bpm = @"bpm";
static NSString* NNOiX_Key_steps = @"steps";
static NSString* NNOiX_Key_sampleDate = @"sampleDate";

@implementation SyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _hkManager = [NNHKManager sharedManager];

    _hkAccessRequested = NO;
    
    [_spinnerView setHidesWhenStopped:YES];
    [_spinnerView stopAnimating];
    _debugPrint = NO;
    [_printButton setTitle:@"🤓JSON" forState:UIControlStateNormal];
    _statusTextView.text = @"**** Welcome to the OiX Cloud ****\n"\
    "This app will pull 7 days of Heart, Step and Workout data from your Health App and push it up to the OiX Cloud.\n"\
    "By using this app you agree to the Terms of Service.\n\n"\
    "1. SYNC data to OiX Cloud from HealthKit. 👆\n"\
    "2. PULL data from OiX Cloud for verification. 👇\n"\
    "3. SHARE data in CSV, PDF and JSON formats.";
    
    _syncView.layer.cornerRadius = 10;
    _showButton.layer.cornerRadius = 10;
    _printButton.layer.cornerRadius = 10;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(resetFirstRun:)];
    [_printButton addGestureRecognizer:longPress];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    BOOL isSignedIn = [[NNOixCloud sharedCloud] isSignedIn];
    [self enableButtons: isSignedIn];
    if (!isSignedIn) {
        [self showLoginMessage];
    }
}

- (void)showLoginMessage {
    [UIUtils showSignInRequest:self];
}

- (void)resetFirstRun:(UILongPressGestureRecognizer*)gesture {
    if ( gesture.state == UIGestureRecognizerStateEnded ) {
        [[NovoOixUserDefaults sharedDefaults] putFirstRun:NO];
        [[NovoOixUserDefaults sharedDefaults] putAgreedToTermsOfService:NO];
        [[NovoOixUserDefaults sharedDefaults] putRequestedHealthKitAccess:NO];
        [[NovoOixUserDefaults sharedDefaults] putUserId:nil];
    }
}


- (IBAction)prettyPrint:(id)sender {
    _debugPrint = !_debugPrint;
    [self updateOutputView];
}

- (void)updateOutputView {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!_lastHeartbeatJsonResponse && !_lastWorkoutJsonResponse && !_lastStepsJsonResponse) {
            _statusTextView.text = [_statusTextView.text stringByAppendingFormat:@"\n\n*** No data available. Please select the 'Sync HealthKit to OiX Cloud' button above, then select the Pull button below to fetch data from the OiX cloud.\n"];
        } else if (_debugPrint) {
            [_printButton setTitle:@"❤️⚽️👟" forState:UIControlStateNormal];
            NSMutableString* statusText = [NSMutableString string];
            
            if (_lastWorkoutJsonResponse) {
                [statusText appendFormat:@"\n⚽️ WORKOUTS:\n %@", _lastWorkoutJsonResponse];
            }
            if (_lastHeartbeatJsonResponse) {
               [statusText appendFormat:@"\n❤️ HEARTBEATS:\n %@", _lastHeartbeatJsonResponse];
            }
            if (_lastStepsJsonResponse) {
                [statusText appendFormat:@"\n👟 STEPS:\n %@", _lastStepsJsonResponse];
            }
            _statusTextView.text = statusText;
        } else {
            [_printButton setTitle:@"🤓JSON" forState:UIControlStateNormal];
            [self prettyPrintJSON];
        }
    });
}

- (void)prettyPrintJSON {
    
    NSMutableString* res= [NSMutableString stringWithString:@"------------------⚽️-----------------------\n"];
    NSString* userId=nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateFormat = @"MMM dd, YYYY HH:mm";
    
    for (NSDictionary* workoutMap in _lastWorkoutJsonResponse) {
        if (userId == nil) {
            userId = workoutMap[@"userId"]; // Just get this value once?
        }
        
        NSString* workoutName = workoutMap[NNOiX_Key_workoutName];
        NSNumber* workoutType = workoutMap[NNOiX_Key_workoutType];
        NSNumber* startDateNumber = workoutMap[NNOiX_Key_startDate];
        NSNumber* endDateNumber = workoutMap[NNOiX_Key_endDate];
        NSInteger duration = (((NSNumber*)workoutMap[NNOiX_Key_duration]).integerValue)/60; // in minutes
        NSNumber* totalDistance = workoutMap[NNOiX_Key_totalDistance];
        NSNumber* totalEnergyBurned = workoutMap[NNOiX_Key_totalEnergyBurned];
        
        NSString* startDate = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:startDateNumber.doubleValue/1000]];
        if (duration <= 1 && startDateNumber.doubleValue != endDateNumber.doubleValue) {
            // calculate the duration
            duration = (endDateNumber.doubleValue - startDateNumber.doubleValue)/(60*1000); // ms->minutes
        }
        
        NSString *emojiWorkout = [NNHKUtils emojiForType:workoutType.integerValue];
        
        NSString* sum = [NSString stringWithFormat:@"%@ %@:\n\t%@ (%ld minutes)\n\tdistance: %ld m, kcal=%d\n",
                         emojiWorkout,
                         workoutName,
                         startDate,
                         (long)duration,
                         totalDistance.longValue,
                         totalEnergyBurned.intValue
                         ];
        [res appendString:sum];
    }
    
    [res appendString:@"\n----------------❤️------------------\n"];
    [res appendString:[self summarizeHeartrates:_lastHeartbeatJsonResponse]];
    [res appendString:@"\n----------------👟------------------\n"];
    [res appendString:[self summarizeSteps:_lastStepsJsonResponse]];

    _statusTextView.text = res;
}

- (NSString*)summarizeSteps:(NSArray*)stepsArray {
    // Aggregate the steps into hours
    int intervalTime = 24*60*60; // every day
    NSDate *curDate = nil;
    int stepCount=0;
    NSMutableString* res = [NSMutableString string];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateFormat = @"M/d";
    
    for (NSDictionary* map in stepsArray) {
        NSNumber* steps = map[NNOiX_Key_steps];
        NSNumber* startDateNumber = map[NNOiX_Key_sampleDate];
        NSDate* startDate = [NSDate dateWithTimeIntervalSince1970:startDateNumber.doubleValue/1000];
        
        if (curDate == nil || (curDate.timeIntervalSince1970 - startDate.timeIntervalSince1970) < intervalTime) {
            if (curDate == nil) {
                curDate = startDate;
            }
            stepCount += steps.intValue;
        } else {
            NSString* sum = [NSString stringWithFormat:@"👟 steps %3d @%@\n", stepCount, [dateFormatter stringFromDate:curDate] ];
            [res appendString:sum];
            curDate = startDate;
            stepCount = steps.intValue;
        }
    }
    
    if (curDate) {
        NSString* sum = [NSString stringWithFormat:@"👟 steps %3d @%@\n", stepCount, [dateFormatter stringFromDate:curDate] ];
        [res appendString:sum];
    }
    return res;
}

- (NSString*)summarizeHeartrates:(NSArray*)bpmArray {
    // Aggregate the steps into hours
    int intervalTime = 6*60*60; // every six hours
    NSDate *curDate = nil;
    NSDate *lastDate = nil;
    int minBpm= INT_MAX;
    int maxBpm= INT_MIN;
    NSMutableString* res = [NSMutableString string];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateFormat = @"M/d HH:mm";
    
    for (NSDictionary* heartMap in bpmArray) {
        NSNumber* bpmNumber = heartMap[NNOiX_Key_bpm];
        NSNumber* startDateNumber = heartMap[NNOiX_Key_sampleDate];
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:startDateNumber.doubleValue/1000];
        
        int bpm = bpmNumber.intValue;
        
        if (curDate == nil || (curDate.timeIntervalSince1970 - startDate.timeIntervalSince1970) < intervalTime) {
            if (curDate == nil) {
                curDate = startDate;
            }
            lastDate = startDate;
            minBpm = MIN(minBpm, bpm);
            maxBpm = MAX(maxBpm, bpm);
        } else {
            [res appendString:[NSString stringWithFormat:@"❤️ BPM %3d-%3d  %@ to %@\n", minBpm, maxBpm, [dateFormatter stringFromDate:lastDate], [dateFormatter stringFromDate:curDate]]];
            curDate = startDate;
            lastDate = startDate;
            minBpm = bpm;
            maxBpm = bpm;
        }
        
    }
    
    // Add last sample
    if (curDate) {
        [res appendString:[NSString stringWithFormat:@"❤️ BPM %d-%d  %@ to %@\n", minBpm, maxBpm, [dateFormatter stringFromDate:lastDate], [dateFormatter stringFromDate:curDate]]];
    }
    return res;
}

- (IBAction)shareCloudData:(id)sender {
    ShowExportViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowStatsViewController"];
    vc.delegate = self;
    //    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    //    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (IBAction)syncWithOiX:(id)sender {

    if (![_hkManager isHealthDataAvailableAndAuthorized] && !_hkAccessRequested) {
        
        _hkAccessRequested = YES;
        [_hkManager requestAuthorizationWithCompletion:^(BOOL success, NSError * _Nullable error) {
            // Always returns YES for success even though user has denied access
            dispatch_async(dispatch_get_main_queue(), ^{
                [self syncWithOiX:nil];
            });
        }];
        return;
    }
    
    [self enableButtons:NO];
    _statusTextView.text = @"Syncing data...";
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        // Notify when all of the groups have completed
        dispatch_group_t group = dispatch_group_create();
        
        dispatch_group_enter(group);
        [_hkManager pushWorkoutsToOiX:^(BOOL status, NSError *error) {
            NSLog(@"Completed sync with status=%@",error);
            dispatch_group_leave(group);
            dispatch_async(dispatch_get_main_queue(), ^{
                _statusTextView.text = [_statusTextView.text stringByAppendingString:@"\nFinished syncing workout data.\n"];
            });

        }];
        
        dispatch_group_enter(group);
        [_hkManager pushHeartbeatsToOix:^(BOOL status, NSError *error) {
            NSLog(@"Completed sync with status=%@",error);
            dispatch_group_leave(group);
            dispatch_async(dispatch_get_main_queue(), ^{
                _statusTextView.text = [_statusTextView.text stringByAppendingString:@"\nFinished syncing heartbeat data.\n"];
            });

        }];
        
        dispatch_group_enter(group);
        [_hkManager pushStepsToOix:^(BOOL status, NSError *error) {
            NSLog(@"Completed sync with status=%@",error);
            dispatch_group_leave(group);
            dispatch_async(dispatch_get_main_queue(), ^{
                _statusTextView.text = [_statusTextView.text stringByAppendingString:@"\nFinished syncing steps data.\n"];
            });
        }];
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self enableButtons:YES];
        });
    });
    
    
}

- (IBAction)fetchOiX:(id)sender {
    [self enableButtons:NO];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        __block NSString* lastError = nil;
        dispatch_group_t group = dispatch_group_create();
        
        dispatch_group_enter(group);
        [[NNOixCloud sharedCloud] fetchActivityDataWithUserId:nil completion:^(NSArray *jsonResponse, NSError *error) {
            dispatch_group_leave(group);
            if (jsonResponse != nil && error == nil) {
                _lastWorkoutJsonResponse = jsonResponse;
            } else if (error != nil) {
                lastError = [NSString stringWithFormat:@"Status: ERROR\n %@", error];
            }
        }];
        
        dispatch_group_enter(group);
        [[NNOixCloud sharedCloud] fetchHeartDataWithUserId:nil completion:^(NSArray *jsonResponse, NSError *error) {
            dispatch_group_leave(group);
            if (jsonResponse != nil && error == nil) {
                _lastHeartbeatJsonResponse = jsonResponse;
            } else if (error != nil) {
                lastError = [NSString stringWithFormat:@"Status: ERROR\n %@", error];
            }
        }];
        
        dispatch_group_enter(group);
        [[NNOixCloud sharedCloud] fetchStepsWithUserId:nil completion:^(NSArray *jsonResponse, NSError *error) {
            dispatch_group_leave(group);
            if (jsonResponse != nil && error == nil) {
                _lastStepsJsonResponse = jsonResponse;
            } else if (error != nil) {
                lastError = [NSString stringWithFormat:@"Status: ERROR\n %@", error];
            }
        }];
        
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (lastError != nil) {
                    _statusTextView.text = lastError;
                }
                [self updateOutputView];
                [self enableButtons:YES];
            });
        });
        
    });

}

- (void)enableButtons:(BOOL)enable {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (enable) {
            [_spinnerView stopAnimating];
        } else {
            [_spinnerView startAnimating];
        }
        [_syncButton setEnabled:enable];
        [_showButton setEnabled:enable];
        if (enable) {
            [_syncButton setAlpha:1];
            [_showButton setAlpha:1];
        } else {
            [_syncButton setAlpha:0.75];
            [_showButton setAlpha:0.75];
        }
    });
}

#pragma mark - share json

- (void)shareHeartrateDataFromView:(UIViewController*)parentVC {
    
    if (_lastHeartbeatJsonResponse == nil && _lastWorkoutJsonResponse == nil && _lastStepsJsonResponse == nil) {
        return;
    }
    NSMutableArray* shareData = [NSMutableArray arrayWithCapacity:3];
    if (_lastStepsJsonResponse) {
        [shareData addObject:@{ShareManagerKey_fileBaseName : @"NovoOiX_Steps", ShareManagerKey_jsonArray:_lastStepsJsonResponse}];
    }
    if (_lastWorkoutJsonResponse) {
        [shareData addObject:@{ShareManagerKey_fileBaseName : @"NovoOiX_Activity", ShareManagerKey_jsonArray:_lastWorkoutJsonResponse}];
    }
    if (_lastHeartbeatJsonResponse) {
        [shareData addObject:@{ShareManagerKey_fileBaseName : @"NovoOiX_Heartrate", ShareManagerKey_jsonArray:_lastHeartbeatJsonResponse}];
    }
    
    [[ShareManager sharedManager] shareData:shareData options:_shareOptions fromView:self completion:^{
         ;
     }];
}

#pragma mark - ShowExport delegate
-(void) shareWithOptions:(ShareFileTypes)options {
    _shareOptions = options;
     [self shareHeartrateDataFromView:self];
}


@end
