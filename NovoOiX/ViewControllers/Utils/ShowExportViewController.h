//
//  ShowExportViewController.h
//  NovoOiX
//
//  Created by ctby on 3/5/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareManager.h"

@protocol ShowExportViewControllerDelegate
@required
-(void) shareWithOptions:(ShareFileTypes)options;
@end

@interface ShowExportViewController : UIViewController

@property (nonatomic, weak) id<ShowExportViewControllerDelegate> delegate;

@end
