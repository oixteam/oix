//
//  PopupViewController.m
//  NovoOiX
//
//  Created by ctby on 3/5/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "PopupViewController.h"

@interface PopupViewController () {
    
    IBOutlet UISlider *_slider;
    IBOutlet UIView *_popupView;
}

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _popupView.layer.cornerRadius = 10;
    _popupView.layer.masksToBounds = YES;
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addRating:(id)sender {
    if (_delegate) {
        [_delegate ratingLevel:_slider.value * 10];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
