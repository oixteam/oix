//
//  PopupViewController.h
//  NovoOiX
//
//  Created by ctby on 3/5/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PopupViewControllerDelegate

- (void)ratingLevel:(NSInteger)rating;

@end

@interface PopupViewController : UIViewController

@property (nonatomic, weak) id<PopupViewControllerDelegate> delegate;

@end
