//
//  ShowExportViewController.m
//  NovoOiX
//
//  Created by ctby on 3/5/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "ShowExportViewController.h"

@interface ShowExportViewController ()
{
    IBOutlet UIView *_popupView;
    IBOutlet UISwitch *_csvSwitch;
    IBOutlet UISwitch *_jsonSwitch;
    IBOutlet UISwitch *_pdfSwitch;
}

@end

@implementation ShowExportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _popupView.layer.cornerRadius = 10;
    _popupView.layer.masksToBounds = YES;
    _pdfSwitch.on = NO;
    _jsonSwitch.on = NO;
    _csvSwitch.on = YES;
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addShareOptions:(id)sender {

    if (_delegate) {
        ShareFileTypes options = 0;
        if ([_csvSwitch isOn]) {
            options |= kShare_CSV;
        }
        if ([_jsonSwitch isOn]) {
            options |= kShare_JSON;
        }
        if ([_pdfSwitch isOn]) {
            options |= kShare_PDF;
        }
        [_delegate shareWithOptions:options];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
