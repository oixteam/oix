//
//  UIUtils.m
//  NovoOiX
//
//  Created by ctby on 3/2/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "UIUtils.h"


@implementation UIUtils

+ (void)showAlertWithTitle:(NSString*)title message:(NSString*)message fromVC:(UIViewController*)vc
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [vc presentViewController:alert animated:YES completion:nil];
}

+ (void)openHealthKitConfirmationFromVC:(UIViewController*)vc
{
    NSString* title = @"Open Health App";
    NSString* message = @"To change health data sharing permissions, open the Health App. Then select:\nSources->\n NovoOiX->\n  Turn All Categories On";
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* openAction = [UIAlertAction actionWithTitle:@"Open" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"x-apple-health://"] options:@{} completionHandler:nil];
                                                       }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                         }];
    [alert addAction:openAction];
    [alert addAction:cancelAction];
    [vc presentViewController:alert animated:YES completion:nil];
}

+ (void)showSignInRequest:(UIViewController*)vc
{
    NSString* title = @"Sign In";
    NSString* message = @"Please Sign In to access health cloud.";
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* openAction = [UIAlertAction actionWithTitle:@"Sign In" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           // Navigate to login
                                                           [self launchView:@"LoginViewController" fromVC:vc];
                                                       }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                         }];
    [alert addAction:openAction];
    [alert addAction:cancelAction];
    [vc presentViewController:alert animated:YES completion:nil];
}

+ (NSString*)versionString {
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * versionBuildString = [NSString stringWithFormat:@"Version: %@ (%@)", appVersionString, appBuildString];
    return versionBuildString;
}


+ (void)launchView:(NSString*)vcName fromVC:(UIViewController*)parentVC {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:vcName];
    
    if (parentVC.navigationController) {
        [parentVC.navigationController pushViewController:vc animated:YES];
    } else {
        [parentVC presentViewController:vc animated:YES completion:nil];
    }
}


@end
