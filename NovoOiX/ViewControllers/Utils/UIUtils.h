//
//  UIUtils.h
//  NovoOiX
//
//  Created by ctby on 3/2/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIUtils : NSObject
+ (void)showAlertWithTitle:(NSString*)title message:(NSString*)message fromVC:(UIViewController*)vc;
+ (void)openHealthKitConfirmationFromVC:(UIViewController*)vc;
+ (NSString*)versionString;
+ (void)showSignInRequest:(UIViewController*)vc;

@end
