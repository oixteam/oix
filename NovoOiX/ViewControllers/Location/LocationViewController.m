//
//  LocationViewController.m
//  NovoOiX
//
//  Created by ctby on 3/16/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "LocationViewController.h"
#import "SharePreviewViewController.h"
#import "ShareInfo.h"
#import "UIUtils.h"
@import MapKit;
@import CoreLocation;

@interface LocationViewController () <CLLocationManagerDelegate>
{
    IBOutlet MKMapView *_mapView;
    IBOutlet UIButton *_trackingButton;
    ShareInfo *_shareInfo;
    NSMutableArray *_samples;
    BOOL _isTrackingLocation;
    IBOutlet UILabel *_titleLabel;
    NSString *_locationsFilePath;
}

// Location manager must be a strong reference
// * Create an instance of the CLLocationManager class and store a strong reference to it somewhere in your app.
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _shareInfo = [[ShareInfo alloc] initWithName:@"📍Locations" shareType:HKShareType_Location];
    _samples = [NSMutableArray array];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.distanceFilter = 20; // 20 meters?
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.delegate = self;

    [self checkAuthorizationStatus];

    //Zoom to user location
    CLLocationCoordinate2D noLocation = CLLocationCoordinate2DMake(0, 0);
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 1200, 1200);
    [_mapView setRegion:viewRegion animated:YES];
    
    // construct the locations path in the documents directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _locationsFilePath = [paths.firstObject stringByAppendingPathComponent:@"locations.txt"];
    
    [self readLocationsFromFile:_locationsFilePath];

    // For iOS9 we have to call this method if we want to receive location updates in background mode
    if([_locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
        [_locationManager setAllowsBackgroundLocationUpdates:YES];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    SharePreviewViewController* vc = segue.destinationViewController;
    _shareInfo.samples = _samples;
    vc.shareInfo = _shareInfo;
}

#pragma mark - Core location

- (void)checkAuthorizationStatus {
    // Get current authorization status and check it
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (![CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [UIUtils showAlertWithTitle:@"Location Services Unavailable" message:@"Device does not have location tracking that would not drain the battery, so location services are not available." fromVC:self];
        return;
    }
    if (status == kCLAuthorizationStatusNotDetermined) {
        [_locationManager requestAlwaysAuthorization];
    } else if (status == kCLAuthorizationStatusAuthorizedAlways) {
        [self startTracking:YES];
    } else {
        // Display message about disabled location service
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Services" message:@"Go to Settings to enable tracking. Select 'Always' to log significant location changes." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:^(BOOL success) {
                ;
            }];
        }];
        [alertController addAction:settingsAction];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (IBAction)startTrackingLocations:(id)sender {
    [self startTracking:!_isTrackingLocation];
}

- (void)startTracking:(BOOL)trackLocations {
    _isTrackingLocation = trackLocations;
    if (_isTrackingLocation) {
        [_trackingButton setTitle:@"Stop Tracking" forState:UIControlStateNormal];
        [_trackingButton setBackgroundColor:[UIColor colorWithRed:220/256.0 green:20/256.0 blue:20/256.0 alpha:1]];
        [self startSignificantChangeUpdates];
    } else {
        [_trackingButton setTitle:@"Start Tracking" forState:UIControlStateNormal];
        [_trackingButton setBackgroundColor:[UIColor colorWithRed:20/256.0 green:220/256.0 blue:20/256.0 alpha:1]];
        [self stopTrackingLocation];
    }
}

- (void)startSignificantChangeUpdates
{
    [_locationManager startMonitoringSignificantLocationChanges];
}

- (void)stopTrackingLocation {
    [_locationManager stopUpdatingLocation];
}

#pragma mark - location delegate method
/*
 *  locationManager:didUpdateLocations:
 *
 *  Discussion:
 *    Invoked when new locations are available.  Required for delivery of
 *    deferred locations.  If implemented, updates will
 *    not be delivered to locationManager:didUpdateToLocation:fromLocation:
 *
 *    locations is an array of CLLocation objects in chronological order.
 */
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    [self addNewLocations:locations];

    // Defer updates until the user hikes a certain distance
    // or when a certain amount of time has passed.
//    if (!_deferringUpdates) {
//        CLLocationDistance distance = self.hike.goal - self.hike.distance;
//        NSTimeInterval time = [self.nextAudible timeIntervalSinceNow];
//        [_locationManager allowDeferredLocationUpdatesUntilTraveled:distance
//                                                            timeout:time];
//        _deferringUpdates = YES;
//    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        [self startTracking:YES];
    }
}

#pragma mark - Add map locations

- (void)addMapLocation:(CLLocation*)location {
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = location.coordinate;
    [_mapView addAnnotation:annotation];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance( location.coordinate, 1200, 1200);
    [_mapView setRegion:viewRegion animated:YES];
    _titleLabel.text = [NSString stringWithFormat:@"Location Sample Count: %lu", (unsigned long)_samples.count];
}

- (void)addNewLocations:(NSArray<CLLocation *> *)locations {

    for (CLLocation *loc in locations) {
        CLLocationDegrees latitude = loc.coordinate.latitude;
        CLLocationDegrees longitude = loc.coordinate.longitude;
        NSDate *sampleDate = [NSDate date];
        
        // Add to list for sharing
        NSDictionary* sampleData = @{ @"latitude":@(latitude),
                                      @"longitude":@(longitude),
                                      @"sampleDate" : @((long long) sampleDate.timeIntervalSince1970 * 1000)
                                      };
        [_samples addObject:sampleData];
        
        // Add to UI
        [self addMapLocation:loc];
        
        // Save it to disk, persist data
        [self addLocationSample:sampleData];
        
       // [self resolveLocation:loc];
    }
}

// Fetch the locations from a file. TODO move this to coredata
- (void)readLocationsFromFile:(NSString*)locationsFilePath {
    
    NSArray *sampleLocations = [NSArray arrayWithContentsOfFile:locationsFilePath];
    
    // Add these to the samples
    [_samples addObjectsFromArray:sampleLocations];
    
    // Convert location dictionaries to CLLocation objects and add to map
    for (NSDictionary *dict in sampleLocations) {
        NSNumber *latitudeNumber = dict[@"latitude"];
        NSNumber *longitudeNumber = dict[@"longitude"];
        CLLocationDegrees latitude = latitudeNumber.doubleValue;
        CLLocationDegrees longitude = longitudeNumber.doubleValue;
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude
                                                          longitude:longitude];
        [self addMapLocation:location];
    }
}

- (void)addLocationSample:(NSDictionary*)sampleData {
    NSMutableArray *locations = [[NSArray arrayWithContentsOfFile:_locationsFilePath] mutableCopy];
    if (!locations) {
        locations = [NSMutableArray new];
    }
    [locations addObject:sampleData];
    [locations writeToFile:_locationsFilePath atomically:YES];
}


@end
