//
//  ShareViewController.m
//  NovoOiX
//
//  Created by ctby on 3/15/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "ShareViewController.h"
#import "ShareInfo.h"
#import "SharePreviewViewController.h"
#import "NNHKManager.h"
#import "UIUtils.h"


@interface ShareViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSArray* _shareItems;
    IBOutlet UITableView *_tableView;
}

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shareItems = @[
                    [[ShareInfo alloc] initWithName:@"❤️ Heart Rate" shareType:HKShareType_HeartRate],
                    [[ShareInfo alloc] initWithName:@"👟 Steps" shareType:HKShareType_Steps],
                    [[ShareInfo alloc] initWithName:@"🏋🏻‍♀️ Workouts" shareType:HKShareType_Workout],
                    [[ShareInfo alloc] initWithName:@"🍎 Nutrition" shareType:HKShareType_Nutrition],
                    ];
    [self checkHKAuth];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self isAuthorized]) {
        [self fetchHealthKitData];
    }
}

- (void)checkHKAuth {
    
    if (![self isAuthorized]) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Health App Permissions"
                                                                       message:@"You must turn on all categories to allow HealthKit data sharing."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* allowAction = [UIAlertAction actionWithTitle:@"Allow" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [[NNHKManager sharedManager]  requestAuthorizationWithCompletion:^(BOOL success, NSError * _Nullable error) {
                                                                      // Always returns YES for success even though user has denied access
                                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                                          if ([self isAuthorized]) {
                                                                              [self fetchHealthKitData];
                                                                          } else {
                                                                              [UIUtils openHealthKitConfirmationFromVC:self];                                                                          }
                                                                      });
                                                                  }];
                                                              }];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                             }];
        [alert addAction:allowAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}


- (void)fetchHealthKitData {
    
    [[NNHKManager sharedManager] shareHeartRateWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
        ((ShareInfo*)_shareItems[0]).samples = samples;
        [self updateTable];
    }];
    [[NNHKManager sharedManager] shareStepsWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
        ((ShareInfo*)_shareItems[1]).samples = samples;
        [self updateTable];
    }];
    [[NNHKManager sharedManager] shareWorkoutsWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
        ((ShareInfo*)_shareItems[2]).samples = samples;
        [self updateTable];
    }];
    [[NNHKManager sharedManager] shareNutritionWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
        ((ShareInfo*)_shareItems[3]).samples = samples;
        [self updateTable];
    }];
}
- (BOOL) isAuthorized {
    return [[NNHKManager sharedManager] isHealthDataAvailableAndAuthorized];
}

- (void)updateTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_tableView reloadData];
    });
}

#pragma mark - Navigation

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ShareTableViewCell" forIndexPath:indexPath];
    ShareInfo* shareInfo = _shareItems[indexPath.row];
    cell.textLabel.text = shareInfo.shareName;
    if (shareInfo.samples.count > 0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Sample count: %ld", shareInfo.samples.count];
    } else {
        cell.detailTextLabel.text = @""; // TODO fetch HK data, put count and date here
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _shareItems.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SharePreviewViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePreviewViewController"];
    vc.shareInfo = _shareItems[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
