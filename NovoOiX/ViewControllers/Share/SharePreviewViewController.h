//
//  SharePreviewViewController.h
//  NovoOiX
//
//  Created by ctby on 3/15/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareInfo.h"

@interface SharePreviewViewController : UIViewController

@property (nonatomic, strong) ShareInfo *shareInfo;
@end
