//
//  SharePreviewViewController.m
//  NovoOiX
//
//  Created by ctby on 3/15/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "SharePreviewViewController.h"
#import "NNHKManager.h"
#import "ShareManager.h"
#import "ShowExportViewController.h"

@interface SharePreviewViewController () <UITableViewDataSource, ShowExportViewControllerDelegate>
{
    IBOutlet UITableView *_tableView;
    NSArray *_samples;
    NNHKManager* _hkManager;
    NSDateFormatter* _dateFormatter;
    IBOutlet UILabel *_headerLabel;
    NSString* _headerString;
}

@end

@implementation SharePreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hkManager  = [NNHKManager sharedManager];
    _dateFormatter = [NSDateFormatter new];
    _dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _dateFormatter.dateFormat = @"MMM/dd, HH:mm";
    
    [self fetchSamples];
}

- (void)fetchSamples {
    
    _samples = _shareInfo.samples; // default, like from locations
    _headerString = [NSString stringWithFormat:@"%@ (%ld)", _shareInfo.shareName, _samples.count];
    _headerLabel.text = _headerString;
    
//    if (_shareInfo.shareType == HKShareType_Steps) {
//        [_hkManager shareStepsWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
//            _samples = samples;
//            _headerString = [NSString stringWithFormat:@"%@: %ld",  _shareInfo.shareName, _samples.count];
//            [self updateTable];
//        }];
//    } else if (_shareInfo.shareType == HKShareType_HeartRate) {
//        [_hkManager shareHeartRateWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
//            _samples = samples;
//            _headerString = [NSString stringWithFormat:@"%@: %ld",  _shareInfo.shareName, _samples.count];
//            [self updateTable];
//        }];
//    } else if (_shareInfo.shareType == HKShareType_Workout) {
//        [_hkManager shareWorkoutsWithCompletion:^(NSArray * _Nullable samples, NSError * _Nullable error) {
//            _samples = samples;
//            _headerString = [NSString stringWithFormat:@"%@: %ld",  _shareInfo.shareName, _samples.count];
//            [self updateTable];
//        }];
//    } else if (_shareInfo.shareType == HKShareType_Nutrition) {
//
//    } else if (_shareInfo.shareType == HKShareType_Location){
//        // TODO
//    }
}

- (void)updateTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        _headerLabel.text = _headerString;
        [_tableView reloadData];
    });
}

- (IBAction)shareHealthKitData:(id)sender {
    ShowExportViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowStatsViewController"];
    vc.delegate = self; // Get options from popup
    [self presentViewController:vc animated:YES completion:nil];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SharePreviewTableCell" forIndexPath:indexPath];
    
    NSDictionary *sample = _samples[indexPath.row];
    NSMutableString* text= [NSMutableString string];
    NSString* titleString = nil;
    NSString* dateString = nil;
    
    for (NSString* key in sample.allKeys) {
        NSString* value = nil;
        if ([sample[key] isKindOfClass:NSNumber.class]) {
            NSNumber* numberValue = sample[key];
            if ([key hasSuffix:@"Date"]) {
                if ([key isEqualToString:@"sampleDate"] || [key isEqualToString:@"startDate"]) {
                    value = [_dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:numberValue.doubleValue/1000]];
                    dateString = value;
                }
                continue;
            } else {
                if ((numberValue.doubleValue - numberValue.intValue) > 0.001) {
                    value = [NSString stringWithFormat:@"%.3f", numberValue.doubleValue ];
                } else {
                    value = numberValue.stringValue;
                }
            }
        } else {
            if (_shareInfo.shareType == HKShareType_Workout) {
                titleString = [NSString stringWithFormat:@"%@: %@\n", key, sample[key]];
                continue;
            }
            value = sample[key];
        }
        if (text.length > 0) {
            [text appendString:@"\n"];
        }
        [text appendFormat:@"%@: %@", key, value];
    }

    if (titleString) {
        [text insertString:titleString atIndex:0];
    }
    cell.textLabel.text = text;
    if (dateString) {
        cell.detailTextLabel.text = dateString;
    } else {
        cell.detailTextLabel.text = @"unknown date";
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _samples.count;
}


#pragma mark - ShowExport delegate
-(void) shareWithOptions:(ShareFileTypes)options {
    if (_samples == nil) {
        // TODO show can't share nothing message
        return;
    }

    NSString* fileName = @"NovoOiX";
    if (_shareInfo.shareType == HKShareType_HeartRate) {
        fileName = [fileName stringByAppendingString:@"_Heartrate"];
    } else if (_shareInfo.shareType == HKShareType_Steps) {
        fileName = [fileName stringByAppendingString:@"_Steps"];
    } else if (_shareInfo.shareType == HKShareType_Workout) {
        fileName = [fileName stringByAppendingString:@"_Activity"];
    } else if (_shareInfo.shareType == HKShareType_Nutrition) {
       fileName = [fileName stringByAppendingString:@"_Nutrition"];
    } else if (_shareInfo.shareType == HKShareType_Location) {
        fileName = [fileName stringByAppendingString:@"_Location"];
    }

    [[ShareManager sharedManager] shareData:@[@{ShareManagerKey_fileBaseName : fileName, ShareManagerKey_jsonArray:_samples}] options:options fromView:self completion:^{
        
    }];
}


@end
