//
//  ShareData.h
//  NovoOiX
//
//  Created by ctby on 3/15/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, HKShareType) {
    HKShareType_HeartRate =  (1<<0),
    HKShareType_Steps = (1<<1),
    HKShareType_Workout = (1<<2),
    HKShareType_Location = (1<<3),
    HKShareType_Nutrition = (1<<4),
};

@interface ShareInfo : NSObject

-(instancetype) initWithName:(NSString*)shareName shareType:(HKShareType)shareType;
@property (nonatomic, copy) NSString *shareName;
@property (nonatomic, assign) HKShareType shareType;
@property (nonatomic, strong) NSArray* samples;

@end
