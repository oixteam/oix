//
//  ShareData.m
//  NovoOiX
//
//  Created by ctby on 3/15/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "ShareInfo.h"

@implementation ShareInfo

-(instancetype) initWithName:(NSString*)shareName shareType:(HKShareType)shareType
{
    if (self = [super init]) {
        _shareName = shareName;
        _shareType = shareType;
    }
    return self;
}

@end
