//
//  AppDelegate.m
//  NovoOiX
//
//  Created by ctby on 1/17/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import "AppDelegate.h"
#import "NNOixCloud.h"
#import "NNHKManager.h"
#import "NovoOixUserDefaults.h"
#import "SetupViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    // Init the cloud
   //[[NNOixCloud sharedCloud] reportAppEvent:@"app_open" startDate:nil endDate:nil completion:nil];

    if ([[NovoOixUserDefaults sharedDefaults] isFirstRun] || ![[NNHKManager sharedManager] isHealthDataAvailableAndAuthorized])
    {
        [self showLoginScreen:YES];
    }
   
    return YES;
}

-(void) showLoginScreen:(BOOL)animated
{
    // Get login screen from storyboard and present it
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SetupViewController *viewController = (SetupViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SetupViewController"];
    UINavigationController* navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.window makeKeyAndVisible];
    [self.window.rootViewController presentViewController:navigation animated:animated completion:nil];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //[[NNOixCloud sharedCloud] reportAppEvent:@"app_close" startDate:nil endDate:nil completion:nil];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    //[[NNOixCloud sharedCloud] reportAppEvent:@"app_open" startDate:nil endDate:nil completion:nil];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
