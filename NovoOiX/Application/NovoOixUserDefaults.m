//
//  NovoOixUserDefaults.m
//  NovoOixrx
//
//  Created by ctby on 9/1/17.
//  Copyright © 2017 ctby. All rights reserved.
//

#import "NovoOixUserDefaults.h"

static NSString* const NovoOixPropertyToSAgree         = @"NovoOixPropertyToSAgree"; // BOOL
static NSString* const NovoOixPropertyNotFirstRun      = @"NovoOixPropertyNotFirstRun"; // BOOL
static NSString* const NovoOixPropertyRequestedHealthKitAccess         = @"NovoOixPropertyRequestedHealthKitAccess"; // BOOL
static NSString* const NovoOixPropertyAgreedToTermsOfService         = @"NovoOixPropertyAgreedToTermsOfService"; // BOOL
static NSString* const NovoOixPropertyUserId         = @"NovoOixPropertyUserId"; // NSString


@implementation NovoOixUserDefaults
{
    BOOL _isFirstRun;
}

+(instancetype)sharedDefaults
{
    static NovoOixUserDefaults *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [NovoOixUserDefaults new];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        _isFirstRun = ![self boolForKey:NovoOixPropertyNotFirstRun];
    }
    return self;
}

-(void)saveString:(NSString*)value forKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)saveInteger:(NSInteger)value forKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] setInteger:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)saveBoolean:(BOOL)value forKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)stringForKey:(NSString*)key defaultValue:(NSString*)defaultValue
{
    NSString* value = [[NSUserDefaults standardUserDefaults] stringForKey:key];
    return (value)?:defaultValue;
}

-(NSInteger)integerForKey:(NSString*)key defaultValue:(NSInteger)defaultValue
{
    NSInteger value = [[NSUserDefaults standardUserDefaults] integerForKey:key];
    return (value)?:defaultValue;
}

-(BOOL)boolForKey:(NSString*)key
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

// Add category for this
- (BOOL)isFirstRun {
    return _isFirstRun;
}
- (void)putFirstRun:(BOOL)firstRun {
    _isFirstRun = firstRun;
    [self saveBoolean:!firstRun forKey:NovoOixPropertyNotFirstRun];
}

- (BOOL)hasRequestedHealthKitAccess {
    return [self boolForKey:NovoOixPropertyRequestedHealthKitAccess];
}
- (void)putRequestedHealthKitAccess:(BOOL)requested {
    [self saveBoolean:requested forKey:NovoOixPropertyRequestedHealthKitAccess];
}

- (BOOL)agreedToTermsOfService {
    return [self boolForKey:NovoOixPropertyAgreedToTermsOfService];
}
- (void)putAgreedToTermsOfService:(BOOL)tosAgreed {
    [self saveBoolean:tosAgreed forKey:NovoOixPropertyAgreedToTermsOfService];
}

- (NSString*)userId {
    return [[NSUserDefaults standardUserDefaults] stringForKey:NovoOixPropertyUserId];
}
- (void)putUserId:(NSString*)userId {
    [self saveString:userId forKey:NovoOixPropertyUserId];
}

@end
