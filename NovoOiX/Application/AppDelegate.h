//
//  AppDelegate.h
//  NovoOiX
//
//  Created by ctby on 1/17/18.
//  Copyright © 2018 ctby. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

