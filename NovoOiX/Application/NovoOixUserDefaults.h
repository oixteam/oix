//
//  NovoOixUserDefaults.h
//  NovoOix
//
//  Created by ctby on 9/1/17.
//  Copyright © 2017 ctby. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSString* const NNPref_AdvancedFeatures = @"NNPref_AdvancedFeatures";

@interface NovoOixUserDefaults : NSObject

+(NovoOixUserDefaults*)sharedDefaults;

-(void)saveString:(NSString*)value forKey:(NSString*)key;
-(void)saveInteger:(NSInteger)value forKey:(NSString*)key;
-(void)saveBoolean:(BOOL)value forKey:(NSString*)key;
-(NSString*)stringForKey:(NSString*)key  defaultValue:(NSString*)defaultValue;
-(NSInteger)integerForKey:(NSString*)key defaultValue:(NSInteger)defaultValue;
-(BOOL)boolForKey:(NSString*)key;

- (BOOL)isFirstRun;
- (void)putFirstRun:(BOOL)fristRun;
- (BOOL)hasRequestedHealthKitAccess;
- (void)putRequestedHealthKitAccess:(BOOL)requested;
- (BOOL)agreedToTermsOfService;
- (void)putAgreedToTermsOfService:(BOOL)tosAgreed;

- (NSString*)userId;
- (void)putUserId:(NSString*)userId;

@end
