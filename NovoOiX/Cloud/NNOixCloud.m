//
//  NNOixCloud.m
//  xurx
//
//  Created by ctby on 8/16/17.
//  Copyright © 2017 ctby. All rights reserved.
//

#import "NNOixCloud.h"
#import "NNHKManager.h"
#import "NNHKUtils.h"
#import "NovoOixUserDefaults.h"

static NSString* NNOixAPI_endpoint = @"http://YOUR_AWS_ENDPOINT"; // AWS. Python code for Elastic Beanstalk, DynamoDB backend provided in subfolder 'backend'

static NSString* NNOixAPIKey = nil;
static NSString* NNOixAPI_fetchOverview = @"fetchOverview";

static NSString* NNOixAPI_registerUser = @"registerUser";

// Heart data, bpm, sampleDate
static NSString* NNOixAPI_addHeartData = @"addHeartData";
static NSString* NNOixAPI_addHeartDataBatch = @"addHeartDataBatch";
static NSString* NNOixAPI_fetchHeartData = @"fetchHeartData";
static NSString* NNOixAPI_addActivityData = @"addActivityData";
static NSString* NNOixAPI_addActivityDataBatch = @"addActivityDataBatch";
static NSString* NNOixAPI_fetchActivityData = @"fetchActivityData";
static NSString* NNOixAPI_addAppEvent = @"addAppEvent";

// Step data
static NSString* NNOixAPI_addStepData = @"addStepData";
static NSString* NNOixAPI_addStepDataBatch = @"addStepDataBatch";
static NSString* NNOixAPI_fetchStepData = @"fetchStepData";

// Keys
static NSString* NNOixKey_userId = @"userId";
static NSString* NNOixKey_adminId = @"adminId";
static NSString* NNOixKey_sampleDate = @"sampleDate";
static NSString* NNOixKey_bpm = @"bpm";
static NSString* NNOixKey_steps = @"steps";


@implementation NNOixCloud
{
    NSDictionary* _overviewInfo;
    NSDate* _overviewFetchDate;
    NSString* _userId;
    NSString* _tokenId;
}

+ (NNOixCloud *)sharedCloud {
    static NNOixCloud *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NNOixCloud alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if ( (self = [super init]) ) {
        _userId = [[NovoOixUserDefaults sharedDefaults] userId];
        NSLog(@"**** User logged in with id: %@", _userId);
    }
    return self;
}

#pragma User

-(void) registerUserWithId:(NSString*)userId email:(NSString*)email userName:(NSString*)userName completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion
{
    // userId
//    createdAt
//    email
//    id
//    tosAgree
//    updatedAt
//    userName
    NSMutableDictionary* userData = [NSMutableDictionary dictionaryWithCapacity:4];
    if (userId != nil) {
        [userData setObject:userId forKey:@"userId"];
    }
    if (userName != nil) {
        [userData setObject:userName forKey:@"userName"];
    }
    if (email != nil) {
        [userData setObject:email forKey:@"email"];
    }
    
    NSLog(@"registerUserWithId post data = %@", userData);
    
    [self postWithAPI:NNOixAPI_registerUser withData:userData completion:^(id jsonResponse, NSError *error) {
        if (error == nil) {
            NSDictionary* userData = jsonResponse;
            _userId = userData[@"userId"];
            NSLog(@"Logged in with userData: %@", userData);
            if (_userId != nil) {
                [[NovoOixUserDefaults sharedDefaults] putUserId:_userId];    
            }
        }
        completion(jsonResponse, error);
    }];
}

- (NSString*)currentUserId {
    return _userId;
}
- (BOOL) isSignedIn {
    return (_userId != nil);
}
- (void) signOut {
    _userId = nil;
    [[NovoOixUserDefaults sharedDefaults] putUserId:nil];
}

- (void)useTesterId {
    if (_userId == nil) {
        _userId = @"7d86c400-f7e9-11e7-adde-a860b63b7809";
    }
}

- (void)signedInWithId:(NSString*)userId idToken:(NSString*)idToken {
    _userId = userId;
    _tokenId = idToken;
}

#pragma mark - Fetch

-(void) fetchActivityDataWithUserId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion
{
    [self fetchRequestWithAPI:NNOixAPI_fetchActivityData userId:userId completion:completion];
}

-(void) fetchHeartDataWithUserId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion
{
    [self fetchRequestWithAPI:NNOixAPI_fetchHeartData userId:userId completion:completion];
}

-(void) fetchStepsWithUserId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion
{
    [self fetchRequestWithAPI:NNOixAPI_fetchStepData userId:userId completion:completion];
}

-(void) fetchRequestWithAPI:(NSString*)apiName userId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion
{
    NSDictionary* userInfo = (userId)?@{NNOixKey_userId:userId}:nil;
    
    [self postWithAPI:apiName withData:userInfo completion:^(id jsonResponse, NSError *error) {
        completion((NSArray*)jsonResponse, error);
    }];
}


#pragma mark - Push data methods

-(void) reportAppEvent:(NSString*)eventName
                     startDate:(NSDate*)startDate
                       endDate:(NSDate*)endDate
                    completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion
{
    // For every sample, we need a sample type, quantity and a date.
    if (startDate ==  nil) {
        startDate = [NSDate date];
    }
    if (endDate ==  nil) {
        endDate = [NSDate date];
    }
    NSDictionary* postData = @{
                               @"eventName" : eventName,
                                  @"startDate"   : @((long long) startDate.timeIntervalSince1970 * 1000),
                                  @"endDate"     : @((long long) endDate.timeIntervalSince1970 * 1000)
                                  };
    
    NSLog(@"HK app open post data = %@", postData);
    
    [self postWithAPI:NNOixAPI_addAppEvent withData:postData completion:completion];
}

-(void) addWorkoutDataWithName:(NSString*)workoutName
                     startDate:(NSDate*)startDate
                       endDate:(NSDate*)endDate
                      duration:(int)duration
                   workoutType:(unsigned int)workoutType
         totalDistanceInMeters:(double)totalDistanceInMeters
             totalEnergyBurned:(double)totalEnergyBurned
                    completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion
{
    
    
    // For every sample, we need a sample type, quantity and a date.
    NSDictionary* workoutData = @{@"workoutName" : workoutName,
                                  @"startDate"   : @((long long) startDate.timeIntervalSince1970 * 1000),
                                  @"endDate"     : @((long long) endDate.timeIntervalSince1970 * 1000),
                                  @"duration"    : @((int)duration),
                                  @"workoutType" : @((unsigned int)workoutType),
                                  @"totalEnergyBurned": @((int)totalEnergyBurned),
                                  @"totalDistance" : @((int)totalDistanceInMeters)
                                   };
    
    NSLog(@"HK workout post data = %@", workoutData);
    
    [self postWithAPI:NNOixAPI_addActivityData withData:workoutData completion:completion];
}

-(void) addHeartbeatWithBPM:(double)bpm
                 sampleDate:(NSDate*)sampleDate
                 completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion
{
    NSDictionary* postData = @{@"bpm" : @((int)bpm),
                               @"sampleDate"   : @((long long) sampleDate.timeIntervalSince1970 * 1000)
                                  };
    
    NSLog(@"HK heart post data = %@", postData);
    
    [self postWithAPI:NNOixAPI_addHeartData withData:postData completion:completion];
}

-(void) addStepCount:(double)steps
                 sampleDate:(NSDate*)sampleDate
                 completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion
{
    NSDictionary* postData = @{@"steps" : @((int)steps),
                               @"sampleDate"   : @((long long) sampleDate.timeIntervalSince1970 * 1000)
                               };
    
    NSLog(@"HK steps post data = %@ (date=%@)", postData, sampleDate);
    
    [self postWithAPI:NNOixAPI_addStepData withData:postData completion:completion];
}
-(void) addStepCounts:(NSArray*)stepsArray
           completion:(void(^)(BOOL status, NSError* error))completion
{
    [self postWithAPI:NNOixAPI_addStepDataBatch withData:@{@"batchedSteps":stepsArray} completion:^(id jsonResponse, NSError *error) {
        completion(YES, error);
    }];
}
-(void) addWorkouts:(NSArray*)workoutsArray
         completion:(void(^)(BOOL status, NSError* error))completion {
    [self postWithAPI:NNOixAPI_addActivityDataBatch withData:@{@"batchedWorkouts":workoutsArray} completion:^(id jsonResponse, NSError *error) {
        completion(YES, error);
    }];
}
-(void) addHeartbeats:(NSArray*)bpmArray
           completion:(void(^)(BOOL status, NSError* error))completion {
    
    // Send in batches of 100
    __block NSError* lastError = nil;
    int pageSize = 300;
    int n=(int)bpmArray.count;
    // Create a serial queue, so that each batch request is complete before the next
    //NSOperationQueue *q = [NSOperationQueue currentQueue];
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    q.maxConcurrentOperationCount = 1;
    
    for (int i=0; i<n; i+=pageSize) {
        NSRange arrayRange = NSMakeRange(i, MIN(n-i, i+pageSize));
        NSArray* batchArray = [bpmArray subarrayWithRange:arrayRange];
        NSLog(@"DISPATCH addHeartbeats batchStart=%d",i);
        [q addOperationWithBlock: ^{
            [self postWithAPI:NNOixAPI_addHeartDataBatch withData:@{@"batchedBpms":batchArray} completion:^(id jsonResponse, NSError *error) {
                NSLog(@"Completed addHeartbeats with status=%@, batchStart=%d",error,i);
                if (error) {
                    lastError = error;
                }
                if (completion && (i+pageSize) >= n) {
                    completion((lastError == nil), lastError);
                }
            }];
            [NSThread sleepForTimeInterval:2.5]; // HACK, based on current limitations for write requests in DynamoDB
        }];
        
    }
    
    
}

///////////////////////////////////////////////////////////////////////
-(void) postWithAPI:(NSString*)apiName withData:(NSDictionary*)postData completion:(void(^)(id jsonResponse, NSError* error))completion
{
    NSString* fetchUrl = [NSString stringWithFormat:@"%@/%@", NNOixAPI_endpoint, apiName];
    if (NNOixAPIKey) {
        fetchUrl = [fetchUrl stringByAppendingFormat:@"?key=%@", NNOixAPIKey];
    }
    
    NSURL* url = [NSURL URLWithString:fetchUrl];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (postData) {
        dictionary = [NSMutableDictionary dictionaryWithDictionary:postData];
    }
    
    NSString* userId = postData[@"userId"];
    if (userId == nil) {
        userId = _userId;
    }

    if (userId != nil) {
        dictionary[@"userId"] = userId;
    }
    
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:kNilOptions
                                                     error:&error];
    request.HTTPBody = data;
    
    if (!error) {
        NSURLSessionDataTask *postDataTask =
        [session dataTaskWithRequest:request
                   completionHandler:^(NSData *data,
                                       NSURLResponse *response,
                                       NSError *error) {
                       id jsonResponse=nil;
                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                       if (data != nil && httpResponse.statusCode == 200 && data.length > 5) { // HACK currently the api is returning "null" as json, so just check for the size
                           NSError *jsonError = nil;
                           jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                           if (jsonError) {
                               NSLog(@"error is %@", [jsonError localizedDescription]);
                               error = jsonError;
                           }
                       } else {
                           // TODO decode the repsonse
                           NSLog(@"ERROR: response=%@ error=%@", response, error);
                       }
                       
                       if (completion) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               completion(jsonResponse, error);
                           });
                       }
                       
                   }];
        [postDataTask resume];
    } else {
        NSLog(@"Problem posting data. json error: %@", error);
        completion(nil, error);
    }
}


@end
