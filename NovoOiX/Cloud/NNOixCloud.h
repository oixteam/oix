//
//  NNOiXCloud.h
//  xurx
//
//  Created by ctby on 8/16/17.
//  Copyright © 2017 ctby. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NNOixCloud : NSObject

+ (NNOixCloud *)sharedCloud;

-(void) registerUserWithId:(NSString*)userId email:(NSString*)email userName:(NSString*)userName completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion;

- (void) signOut;
- (BOOL) isSignedIn;
- (void)signedInWithId:(NSString*)userId idToken:(NSString*)idToken;
- (NSString*)currentUserId;
- (void)useTesterId;

// Add a workout to the NNOiX health cloud
-(void) addWorkoutDataWithName:(NSString*)workoutName
                     startDate:(NSDate*)startDate
                       endDate:(NSDate*)endDate
                      duration:(int)duration
                   workoutType:(unsigned int)workoutType
         totalDistanceInMeters:(double)totalDistanceInMeters
             totalEnergyBurned:(double)totalEnergyBurned
                    completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion;

// Add heart data
-(void) addHeartbeatWithBPM:(double)bpm
                 sampleDate:(NSDate*)sampleDate
                 completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion;

-(void) addStepCount:(double)steps
          sampleDate:(NSDate*)sampleDate
          completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion;

-(void) addWorkouts:(NSArray*)workoutsArray
         completion:(void(^)(BOOL status, NSError* error))completion;
-(void) addHeartbeats:(NSArray*)bpmArray
           completion:(void(^)(BOOL status, NSError* error))completion;
-(void) addStepCounts:(NSArray*)stepsArray
          completion:(void(^)(BOOL status, NSError* error))completion;

// Fetch NNOiX data
-(void) fetchActivityDataWithUserId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion;
-(void) fetchStepsWithUserId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion;
-(void) fetchHeartDataWithUserId:(NSString*)userId completion:(void(^)(NSArray* jsonResponse, NSError* error))completion;

-(void) reportAppEvent:(NSString*)eventName
             startDate:(NSDate*)startDate
               endDate:(NSDate*)endDate
            completion:(void(^)(NSDictionary* jsonResponse, NSError* error))completion;

@end
